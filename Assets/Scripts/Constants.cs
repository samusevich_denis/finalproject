﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants 
{
    public static class Layer
    {
        public static readonly int IgnoreRaycast = 2; 
        public static readonly int Character = 10;
        public static readonly int Enemy = 11;
        public static readonly int Platform = 12;
        public static readonly int SmallPlatform = 13;
        public static readonly int BulletEnemy = 14;
        public static readonly int BulletCharacter = 15;
        public static readonly int ObjectForCharacter = 16;
        public static readonly int ObjectForEnemy = 17;

        public static readonly int Effects = 20;
    }

    public static class AngleForAxisY
    {
        public static readonly float DirectionRight = 0f;
        public static readonly float DirectionalLeft = 180;
    }
    public static class RoomMedieval
    {
        public static readonly Vector3 StartRoomPosition = new Vector3(-0.48f, 0f);
        public static readonly float DistanceVertical = 30f; 
        public static readonly float DistanceHorizontal = 39f; 
    }
    public static class HeroEpsilon
    {
        public static readonly Vector3 PositionRaycastPushTop = new Vector3(0.078f, 0.488f, 0f);
        public static readonly Vector3 PositionRaycastPushMiddle = new Vector3(0.216f, -0.223f, 0f);
        public static readonly Vector3 PositionRaycastPushBottom = new Vector3(0.078f, -0.934f, 0f);

        public static readonly Vector3 PositionHandIdle_000 = new Vector3(0.169f, -0.273f, 0f);
        public static readonly Vector3 PositionHandIdle_045 = new Vector3(0.197f, -0.209f, 0f);
        public static readonly Vector3 PositionHandIdle_090 = new Vector3(0.283f, -0.104f, 0f);
        public static readonly Vector3 PositionHandIdle_135 = new Vector3(0.221f, -0.001f, 0f);
        public static readonly Vector3 PositionHandIdle_180 = new Vector3(0.074f, 0.172f, 0f);

        public static readonly Vector3 PositionHandMove_000 = new Vector3(0.018f, -0.183f, 0f);
        public static readonly Vector3 PositionHandMove_045 = new Vector3(0.018f, -0.152f, 0f);
        public static readonly Vector3 PositionHandMove_090 = new Vector3(0.025f, -0.096f, 0f);
        public static readonly Vector3 PositionHandMove_135 = new Vector3(-0.089f, 0.013f, 0f);
        public static readonly Vector3 PositionHandMove_180 = new Vector3(-0.111f, 0.002f, 0f);

        public static readonly Vector3 PositionPointShotIdle_000 = new Vector3(0f, -0.315f, 0f);
        public static readonly Vector3 PositionPointShotIdle_045 = new Vector3(0.266f, -0.238f, 0f);
        public static readonly Vector3 PositionPointShotIdle_090 = new Vector3(0.441f, 0, 0f);
        public static readonly Vector3 PositionPointShotIdle_135 = new Vector3(0.287f, 0.355f, 0f);
        public static readonly Vector3 PositionPointShotIdle_180 = new Vector3(0f, 0.362f, 0f);

        public static readonly Vector3 PositionPointShotMove_000 = new Vector3(0.042f, -0.42f, 0f);
        public static readonly Vector3 PositionPointShotMove_045 = new Vector3(0.245f, -0.322f, 0f);
        public static readonly Vector3 PositionPointShotMove_090 = new Vector3(0.525f, 0, 0f);
        public static readonly Vector3 PositionPointShotMove_135 = new Vector3(0.343f, 0.259f, 0f);
        public static readonly Vector3 PositionPointShotMove_180 = new Vector3(0.112f, 0.421f, 0f);

        public static readonly Vector3 VectorTranslatePointAngleUp = new Vector3(0f, 0.3f, 0f);
        public static readonly Vector3 VectorTranslatePointAngleDown = new Vector3(0f, -0.3f, 0f);

        public static readonly Vector3 PositionHeroUp = new Vector3(0.049f, 0.185f, 0f);
        public static readonly Vector3 PositionHeroUpCrouchMiddle = new Vector3(0.049f, 0f, 0f);
        public static readonly Vector3 PositionHeroUpCrouchDown = new Vector3(0.049f, -0.145f, 0f);
    }
}
