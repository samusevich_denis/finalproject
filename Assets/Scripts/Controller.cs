﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour
{
    public static Controller GetController() 
    {
        Controller controller = (Controller)FindObjectOfType(typeof(Controller));
        if (controller==null)
        {
            Debug.LogError("Controller not found");
        }
        return controller;
    }
    public abstract void InitScene();
    public abstract void Deactivation();
    public abstract void Activation(); 
}
