﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusWeapon : Bonus
{
    public DataWeapon dataWeapon { get; private set; }

    void Start()
    {
        dataWeapon = DataWeapon.GetDataWeapon(SettingPrefabs.Bonus.Weapon.DataWeapons[Settings.Bonus.Weapon.RandomWeapon],0);
        var spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = dataWeapon.spriteWeapon;
    }
}
