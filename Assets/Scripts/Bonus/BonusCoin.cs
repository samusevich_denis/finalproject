﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum TypeGems
{
    None,
    RubySmall,
    EmeraldSmall,
    TopazSmall,
    DiamondSmall,
    RubyMedium,
    EmeraldMedium,
    TopazMedium,
    DiamondMedium,
    RubyBig,
    EmeraldBig,
    TopazBig,
    DiamondBig,
}

public class BonusCoin : Bonus
{
    public int Coin { get; private set; }
    
    private void Start()
    {
        Coin = GetCoin();
    }

    private int GetCoin()
    {
        var spriteRenderer = GetComponent<SpriteRenderer>();
        var animator = GetComponent<Animator>();
        switch ((TypeGems)Settings.Bonus.Gems.RandomGems)
        {
            case TypeGems.None:
                spriteRenderer.sprite = SettingPrefabs.Bonus.GemsSprite.Coin;
                animator.enabled = true;
                return Settings.Bonus.Gems.Coin;
            case TypeGems.RubySmall:
                spriteRenderer.sprite = SettingPrefabs.Bonus.GemsSprite.RubySmall; 
                return Settings.Bonus.Gems.RubySmall;
            case TypeGems.EmeraldSmall:
                spriteRenderer.sprite = SettingPrefabs.Bonus.GemsSprite.EmeraldSmall;
                return Settings.Bonus.Gems.EmeraldSmall;
            case TypeGems.TopazSmall:
                spriteRenderer.sprite = SettingPrefabs.Bonus.GemsSprite.TopazSmall;
                return Settings.Bonus.Gems.TopazSmall;
            case TypeGems.DiamondSmall:
                spriteRenderer.sprite = SettingPrefabs.Bonus.GemsSprite.DiamondSmall;
                return Settings.Bonus.Gems.DiamondSmall;
            case TypeGems.RubyMedium:
                spriteRenderer.sprite = SettingPrefabs.Bonus.GemsSprite.RubyMedium;
                return Settings.Bonus.Gems.RubyMedium;
            case TypeGems.EmeraldMedium:
                spriteRenderer.sprite = SettingPrefabs.Bonus.GemsSprite.EmeraldMedium;
                return Settings.Bonus.Gems.EmeraldMedium;
            case TypeGems.TopazMedium:
                spriteRenderer.sprite = SettingPrefabs.Bonus.GemsSprite.TopazMedium;
                return Settings.Bonus.Gems.TopazMedium;
            case TypeGems.DiamondMedium:
                spriteRenderer.sprite = SettingPrefabs.Bonus.GemsSprite.DiamondMedium;
                return Settings.Bonus.Gems.DiamondMedium;
            case TypeGems.RubyBig:
                spriteRenderer.sprite = SettingPrefabs.Bonus.GemsSprite.RubyBig;
                return Settings.Bonus.Gems.RubyBig;
            case TypeGems.EmeraldBig:
                spriteRenderer.sprite = SettingPrefabs.Bonus.GemsSprite.EmeraldBig;
                return Settings.Bonus.Gems.EmeraldBig;
            case TypeGems.TopazBig:
                spriteRenderer.sprite = SettingPrefabs.Bonus.GemsSprite.TopazBig;
                return Settings.Bonus.Gems.TopazBig;
            case TypeGems.DiamondBig:
                spriteRenderer.sprite = SettingPrefabs.Bonus.GemsSprite.DiamondBig;
                return Settings.Bonus.Gems.DiamondBig;
        }
        return 0;
    }
}
