﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeBonus
{
    Weapon,
    Health,
    Gems,
}
public class Bonus : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision != null && collision.TryGetComponent<Hero>(out Hero hero))
        {
            Inventory.PickUpBonus(this);
            Destroy(transform.gameObject);
        }
    }

    public static void CreateBonus(Vector3 position)
    {
        var obj = Instantiate(SettingPrefabs.Bonus.StandartBonusPrefab, position, Quaternion.identity);
        switch ((TypeBonus)Settings.Bonus.RandomBonus)
        {
            case TypeBonus.Weapon:
                obj.AddComponent<BonusWeapon>();
                break;
            case TypeBonus.Health:
                obj.AddComponent<BonusHeart>();
                break;
            case TypeBonus.Gems:
                obj.AddComponent<BonusCoin>();
                break;
            default:
                Debug.Log("No bonus added");
                break;
        }
        var rig = obj.GetComponent<Rigidbody2D>();
        rig.AddForce(new Vector2(Random.Range(-1f, 1f), 1f).normalized*5, ForceMode2D.Impulse);
    }
}
