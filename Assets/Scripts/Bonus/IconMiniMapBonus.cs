﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconMinimapBonus : MonoBehaviour
{
    void Start()
    {
        var spriteRen = GetComponent<SpriteRenderer>();
        switch (GetComponentInParent<Bonus>())
        {
            case BonusWeapon weapon:
                spriteRen.color = new Color(1f, 0.5f, 0f, 1f);
                break;
            case BonusHeart heart:
                spriteRen.color = Color.magenta;
                break;
            case BonusCoin coin:
                spriteRen.color = Color.yellow;
                break;
            default:
                Debug.Log("No bonus type found");
                break;
        }
    }
}
