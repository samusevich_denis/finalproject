﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeHeart
{
    Small,
    Medium,
    Big,
}

public class BonusHeart : Bonus
{
    public int Health { get; private set; }

    void Start()
    {
        Health = GetHeart();
    }

    private int GetHeart()
    {
        var spriteRenderer = GetComponent<SpriteRenderer>();
        switch ((TypeHeart)Settings.Bonus.Health.RandomHealth)
        {
            case TypeHeart.Small:
                spriteRenderer.sprite = SettingPrefabs.Bonus.Hearts.Sprites.Small;
                return Settings.Bonus.Health.Small;
            case TypeHeart.Medium:
                spriteRenderer.sprite = SettingPrefabs.Bonus.Hearts.Sprites.Medium;
                return Settings.Bonus.Health.Medium;
            case TypeHeart.Big:
                spriteRenderer.sprite = SettingPrefabs.Bonus.Hearts.Sprites.Big;
                return Settings.Bonus.Health.Big;
            default:
                spriteRenderer.sprite = SettingPrefabs.Bonus.Hearts.Sprites.Small;
                return Settings.Bonus.Health.Small;
        }
    }
}
