﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum StateGame
{
    Init,
    MainMenu,
    Loading,
    Game,
}

public class SceneLoader : MonoBehaviour
{

    private Image fadeImage;
    private float speedFade = Settings.UI.SpeedFadeOutScene;
    private static StateGame nextState;
    private static StateGame currentState;

    private Controller controller;

    public void Awake()
    {
        DontDestroyOnLoad(this);
        if (FindObjectsOfType(GetType()).Length > 1)
            Destroy(gameObject);
    }

    public static SceneLoader GetSceneLoader()
    {
        return (SceneLoader)FindObjectOfType(typeof(SceneLoader));
    }

    void Start()
    {
        fadeImage = GetComponentInChildren<Image>();
        var color = fadeImage.color;
        color.a = 1f;
        fadeImage.color = color;
        fadeImage.gameObject.SetActive(false);
        LoadScene(StateGame.MainMenu);
    }



    public void LoadScene(StateGame scene)
    {
        nextState = scene;
        StartCoroutine(LoadingScene());
    }

    private IEnumerator LoadingScene()
    {
        fadeImage.gameObject.SetActive(true);

        while (fadeImage.color.a < 0.99f)
        {
            var color = fadeImage.color;
            color.a = Mathf.Lerp(color.a, 1, speedFade* Time.fixedDeltaTime);
            fadeImage.color = color;
            yield return new WaitForSecondsRealtime(Time.fixedDeltaTime);
        }
        var colorEnd = fadeImage.color;
        colorEnd.a = 1f;
        fadeImage.color = colorEnd;

        controller?.Deactivation();
        AsyncOperation loadingLoading = SceneManager.LoadSceneAsync((int)StateGame.Loading, LoadSceneMode.Additive);
        while (!loadingLoading.isDone)
        {
            yield return new WaitForSecondsRealtime(Time.fixedDeltaTime);
        }

        while (fadeImage.color.a > 0.01f)
        {
            var color = fadeImage.color;
            color.a = Mathf.Lerp(color.a, 0, speedFade* Time.fixedDeltaTime);
            fadeImage.color = color;
            yield return new WaitForSecondsRealtime(Time.fixedDeltaTime);
        }
        colorEnd = fadeImage.color;
        colorEnd.a = 0f;
        fadeImage.color = colorEnd;

        AsyncOperation unloadingCurrentState = SceneManager.UnloadSceneAsync((int)currentState);
        while (!unloadingCurrentState.isDone)
        {
            yield return new WaitForSecondsRealtime(Time.fixedDeltaTime);
        }

        AsyncOperation loadingNextState = SceneManager.LoadSceneAsync((int)nextState, LoadSceneMode.Additive);
        while (!loadingNextState.isDone)
        {
            yield return new WaitForSecondsRealtime(Time.fixedDeltaTime);
        }

        var scenes = SceneManager.GetAllScenes();
        for (int i = 0; i < scenes.Length; i++)
        {
            if (scenes[i].buildIndex == (int)nextState)
            {
                SceneManager.SetActiveScene(scenes[i]);
                currentState = nextState;
            }

        }

        controller = Controller.GetController();
        controller.InitScene();

        while (fadeImage.color.a < 0.99f)
        {
            var color = fadeImage.color;
            color.a = Mathf.Lerp(color.a, 1, speedFade* Time.fixedDeltaTime);
            fadeImage.color = color;
            yield return new WaitForSecondsRealtime(Time.fixedDeltaTime);
        }
        colorEnd = fadeImage.color;
        colorEnd.a = 1f;
        fadeImage.color = colorEnd;

        AsyncOperation unloadingLoading = SceneManager.UnloadSceneAsync((int)StateGame.Loading);
        while (!unloadingLoading.isDone)
        {
            yield return new WaitForSecondsRealtime(Time.fixedDeltaTime);
        }

        while (fadeImage.color.a > 0.01f)
        {
            var color = fadeImage.color;
            color.a = Mathf.Lerp(color.a, 0, speedFade* Time.fixedDeltaTime);
            fadeImage.color = color;
            yield return new WaitForSecondsRealtime(Time.fixedDeltaTime);
        }
        colorEnd = fadeImage.color;
        colorEnd.a = 0f;
        fadeImage.color = colorEnd;

        fadeImage.gameObject.SetActive(false);
        controller.Activation();
    }
}
