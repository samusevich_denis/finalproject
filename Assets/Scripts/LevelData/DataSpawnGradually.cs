﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataSpawn_Gradually 0", menuName = "ObjectData/DataSpawn/TypeGradually", order = 2)]
public class DataSpawnGradually : DataSpawn
{
    public int minEnemy;
    public int maxEnemy;
    private DataSpawnGradually()
    {
        base.typeSpawn = TypeSpawning.Gradually;
    }
}
