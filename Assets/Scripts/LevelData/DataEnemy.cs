﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataEnemy 0", menuName = "ObjectData/DataEnemy", order = 1)]
public class DataEnemy : ScriptableObject
{
    public int quantityEnemies;
    public GameObject enemyPrefabs;
    public Color colorEnemy;
    public float sizeEnemy;
    public int baseHealthEnemy;
    public DataWeapon dataWeapon;

    public static DataEnemy CopyDataEnemy(DataEnemy dataEnemy)
    {
        var newDataEnemy = CreateInstance<DataEnemy>();
        newDataEnemy.quantityEnemies = dataEnemy.quantityEnemies;
        newDataEnemy.enemyPrefabs = dataEnemy.enemyPrefabs;
        newDataEnemy.colorEnemy = dataEnemy.colorEnemy;
        newDataEnemy.sizeEnemy = dataEnemy.sizeEnemy;
        newDataEnemy.baseHealthEnemy = dataEnemy.baseHealthEnemy;
        newDataEnemy.dataWeapon = dataEnemy.dataWeapon;
        return newDataEnemy;
    }
}
