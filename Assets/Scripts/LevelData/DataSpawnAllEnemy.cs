﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataSpawn_AllEnemy 0", menuName = "ObjectData/DataSpawn/TypeAllEnemy", order = 1)]
public class DataSpawnAllEnemy : DataSpawn
{
    private DataSpawnAllEnemy()
    {
        base.typeSpawn = TypeSpawning.AllEnemy;
    }
}
