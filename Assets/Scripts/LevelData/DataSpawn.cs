﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataSpawn : ScriptableObject
{
    public enum TypeSpawning
    {
        AllEnemy,
        Gradually,
        ByTime,
    }
    public TypeSpawning typeSpawn { get; protected set; }
    public int quantityPortals;
}
