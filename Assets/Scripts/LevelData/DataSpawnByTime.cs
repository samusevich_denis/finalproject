﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataSpawn_ByTime 0", menuName = "ObjectData/DataSpawn/TypeByTime", order = 3)]
public class DataSpawnByTime : DataSpawn
{
    public float timerSpawn;
    public int quantityEnemySpawnForTime;
    private DataSpawnByTime()
    {
        base.typeSpawn = TypeSpawning.ByTime;
    }
}
