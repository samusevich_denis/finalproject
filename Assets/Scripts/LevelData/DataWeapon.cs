﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "DataWeapon 0", menuName = "ObjectData/DataWeapon", order = 1)]
public class DataWeapon : ScriptableObject
{
    public Sprite spriteWeapon;
    public int index;
    public float reloadTime;
    public float shotTime;
    public int damage;
    public GameObject bullet;
    public int countBullet;
    public int quantityBullets;
    public float timerLifeBullet;
    public float speedBullet;
    public float forceBullet;
    public float forcePushEnemy;

    public static DataWeapon GetDataWeapon(DataWeapon dataWeapon, int setIndex)
    {
        var newDataWeapon = CreateInstance<DataWeapon>();
        newDataWeapon.index = setIndex;
        newDataWeapon.reloadTime = dataWeapon.reloadTime;
        newDataWeapon.shotTime = dataWeapon.shotTime;
        newDataWeapon.damage = dataWeapon.damage;
        newDataWeapon.bullet = dataWeapon.bullet;
        newDataWeapon.countBullet = dataWeapon.countBullet;
        newDataWeapon.quantityBullets = dataWeapon.quantityBullets;
        newDataWeapon.timerLifeBullet = dataWeapon.timerLifeBullet;
        newDataWeapon.speedBullet = dataWeapon.speedBullet;
        newDataWeapon.forceBullet = dataWeapon.forceBullet;
        newDataWeapon.forcePushEnemy = dataWeapon.forcePushEnemy;
        newDataWeapon.spriteWeapon = dataWeapon.spriteWeapon;
        return newDataWeapon;
    }
}



