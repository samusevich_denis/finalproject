﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataWave 0", menuName = "ObjectData/DataWave", order = 1)]
public class DataWave : ScriptableObject
{
    public DataEnemy[] enemysData;
    public DataBonus bonusData;
    public DataSpawn spawnData;
    //public BossData bossData; for the future
    //public SecretData secretData; for the future
}
