﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InputControllerGame : InputController
{
    public static Action PauseAction { get; set; }
    public static float HorizontalAxis { get;  set; }
    public static Vector3 MousePosition { get;  set; }
    public static bool ButtonDown { get;  set; }
    public static bool ButtonJumpDown { get;  set; }
    public static bool ButtonMove { get;  set; }
    public static bool MouseButtonDown { get;  set; }
    public static bool ButtonPushDown { get;  set; }
    public static bool ButtonNextWeapon { get;  set; }
    public static bool ButtonReload { get;  set; }
    public static Vector3 MousePositionToWorld
    {
        get => mousePositionToWorld;
        private set
        {
            mousePositionToWorld.x = value.x;
            mousePositionToWorld.y = value.y;
            mousePositionToWorld.z = 0;
        }
    }
    private Coroutine inputCoroutine;
    private static Vector3 mousePositionToWorld;
    private bool triggerButtonJump;
    private bool triggerButtonPush;
    private bool triggerButtonNextWeapon;
    private bool triggerButtonReload;
    private bool triggerButtonPause;
    protected override void Start()
    {
        stateController = StateInputController.Game;
        triggerButtonJump = true;
        triggerButtonPush = true;
        AllInputControllers.Add(this);
        enabled = false;
    }



    void FixedUpdate()
    {
        HorizontalAxis = Input.GetAxis("Horizontal");
        MousePosition = Input.mousePosition;
        MousePositionToWorld = Camera.main.ScreenToWorldPoint(MousePosition);
        ButtonDown = Input.GetKey(KeyCode.S);
        ButtonJumpDown = GetButtonDown(Input.GetKey(KeyCode.Space), ref triggerButtonJump);
        ButtonMove = Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D);
        MouseButtonDown = Input.GetMouseButton(0);
        ButtonPushDown = GetButtonDown(Input.GetKey(KeyCode.LeftShift), ref triggerButtonPush);
        ButtonNextWeapon = GetButtonDown(Input.GetKey(KeyCode.Q), ref triggerButtonNextWeapon);
        ButtonReload = GetButtonDown(Input.GetKey(KeyCode.R), ref triggerButtonReload);

        if (GetButtonDown(Input.GetKey(KeyCode.Escape), ref triggerButtonPause))
        {
            PauseAction?.Invoke();
        }
    }

    protected override void DeactivateController()
    {
        HorizontalAxis = 0;
        ButtonDown = false;
        ButtonJumpDown = false;
        ButtonMove = false;
        MouseButtonDown = false;
        ButtonPushDown = false;
        ButtonReload = false;
        ButtonNextWeapon = false;
        triggerButtonJump = true;
        triggerButtonPush = true;
        enabled = false;
    }

    public void StartInputTutorial()
    {
        inputCoroutine = StartCoroutine(InputTutorial());
    }
    public void StopInputTutorial()
    {
        StopCoroutine(inputCoroutine);
        HorizontalAxis = 0;
        ButtonDown = false;
        ButtonJumpDown = false;
        ButtonMove = false;
        MouseButtonDown = false;
        ButtonPushDown = false;
        ButtonReload = false;
        ButtonNextWeapon = false;
        MousePositionToWorld = Vector3.zero;
    }

    public IEnumerator InputTutorial()
    {
        MousePosition = new Vector3(4f, 0.5f, 0);
        MousePositionToWorld = MenuController.Hero.position + MousePosition;
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(MoveTutorial(-1, 1f, 1f));
        yield return StartCoroutine(MoveTutorial(1, 2f, 1f));
        yield return StartCoroutine(MoveTutorial(-1, 2f, 1f));
        yield return StartCoroutine(CrouchDownTutorial(1.5f, 1f));
        yield return StartCoroutine(CrouchDownTutorial(1.5f, 1f));
        yield return StartCoroutine(JumpTutorial(3f));
        yield return StartCoroutine(JumpTutorial(3f));
        yield return StartCoroutine(JumpFreezeTutorial(0.8f, 2f));
        ButtonJumpDown = true;
        yield return new WaitForFixedUpdate();
        ButtonJumpDown = false;
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(JumpFreezeTutorial(0.8f, 2f));
        ButtonDown = true;
        yield return new WaitForFixedUpdate();
        ButtonDown = false;
        yield return new WaitForSeconds(2f);
        MenuController.ReturnHero();
        MouseButtonDown = true;
        yield return new WaitForSeconds(1f);
        StartCoroutine(MoveMouse());
        yield return StartCoroutine(MoveTutorial(-1, 1f, 1f));
        yield return StartCoroutine(MoveTutorial(1, 2f, 1f));
        yield return StartCoroutine(MoveTutorial(-1, 2f, 1f));
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(CrouchDownTutorial(1.5f, 1f));
        yield return StartCoroutine(JumpTutorial(3f));
        yield return StartCoroutine(JumpTutorial(3f));
        yield return StartCoroutine(JumpFreezeTutorial(0.8f, 2f));
        ButtonJumpDown = true;
        yield return new WaitForFixedUpdate();
        ButtonJumpDown = false;
        yield return new WaitForSeconds(2f);
        MouseButtonDown = false;
        MenuController.Hero.position = new Vector3(-42f, -7f, 0f);
        yield return new WaitForSeconds(3f);
        yield return StartCoroutine(PushTutorial(3f));
        MenuController.Hero.position = new Vector3(-42f, 10f, 0f);
        yield return new WaitForSeconds(3f);
        yield return StartCoroutine(PushTutorial(3f));
        MenuController.Hero.position = new Vector3(-53f, 8.5f, 0f);
        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(MoveTutorial(1, 2f, 1f));
        yield return new WaitForSeconds(1f);
        MousePositionToWorld = MenuController.Hero.position + MousePosition;
        MouseButtonDown = true;
        StartCoroutine(MoveMouse());
        yield return new WaitForSeconds(2f);
        MouseButtonDown = false;
        yield return StartCoroutine(NextWeaponTutorial(1f));
        yield return StartCoroutine(NextWeaponTutorial(1f));
        MouseButtonDown = true;
        StartCoroutine(MoveMouse());
        yield return new WaitForSeconds(2f);
        MouseButtonDown = false;
        yield return StartCoroutine(NextWeaponTutorial(2f));
        MouseButtonDown = true;
        StartCoroutine(MoveMouse());
        yield return new WaitForSeconds(2f);
        MouseButtonDown = false;
        yield return StartCoroutine(NextWeaponTutorial(1f));
        yield return StartCoroutine(NextWeaponTutorial(1f));
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(ReloadTutorial(2f));
        yield return StartCoroutine(NextWeaponTutorial(2f));
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(ReloadTutorial(2f));
    }
    private IEnumerator MoveTutorial(float Axis, params float[] time)
    {
        ButtonMove = true;
        HorizontalAxis = Axis;
        yield return new WaitForSeconds(time[0]);
        ButtonMove = false;
        HorizontalAxis = 0;
        yield return new WaitForSeconds(time[1]);
    }
    private IEnumerator CrouchDownTutorial(params float[] time)
    {
        ButtonDown = true;
        yield return new WaitForSeconds(time[0]);
        ButtonDown = false;
        yield return new WaitForSeconds(time[1]);
    }
    private IEnumerator JumpTutorial( float time)
    {
        ButtonJumpDown = true;
        yield return new WaitForFixedUpdate();
        ButtonJumpDown = false;
        yield return new WaitForSeconds(time);
    }
    private IEnumerator JumpFreezeTutorial(params float[] time)
    {
        ButtonJumpDown = true;
        yield return new WaitForFixedUpdate();
        ButtonJumpDown = false;
        yield return new WaitForSeconds(time[0]);
        ButtonJumpDown = true;
        yield return new WaitForFixedUpdate();
        ButtonJumpDown = false;
        yield return new WaitForSeconds(time[1]);
    }
    private IEnumerator MoveMouse()
    {
        while (MouseButtonDown)
        {
            yield return StartCoroutine(MoveMouseVectorY(new Vector3(0f, 3f, 0f)));
            yield return StartCoroutine(MoveMouseVectorY(new Vector3(0f, -3, 0f)));
        }
    }
    private IEnumerator MoveMouseVectorY(Vector3 vectorMove)
    {
        var posMove = MousePosition + vectorMove;
        while (Mathf.Abs(posMove.y - MousePosition.y) > 0.1)
        {
            MousePosition = Vector3.Lerp(MousePosition, posMove, 0.8f * Time.deltaTime*3);
            MousePositionToWorld = MenuController.Hero.position + MousePosition;
            yield return null;
        }
        MousePosition = posMove;
        MousePositionToWorld = MenuController.Hero.position + MousePosition;
    }
    private IEnumerator PushTutorial(float time)
    {
        ButtonPushDown = true;
        yield return new WaitForFixedUpdate();
        ButtonPushDown = false;
        yield return new WaitForSeconds(time);
    }
    private IEnumerator NextWeaponTutorial(float time)
    {
        ButtonNextWeapon = true;
        yield return new WaitForFixedUpdate();
        ButtonNextWeapon = false;
        yield return new WaitForSeconds(time);
    }
    private IEnumerator ReloadTutorial(float time)
    {
        ButtonReload = true;
        yield return new WaitForFixedUpdate();
        ButtonReload = false;
        yield return new WaitForSeconds(time);
    }

}
