﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum StateInputController
{
    None,
    Game,
    Inventory, //for the future
}

public class InputController : MonoBehaviour
{
    protected static List<InputController> AllInputControllers = new List<InputController>();
    public static InputController currentInputControllers;
    protected StateInputController stateController;

    protected virtual void Start()
    {
        stateController = StateInputController.None;
        AllInputControllers.Add(this);
        currentInputControllers = this;
        enabled = false;
    }

    protected static bool GetButtonDown(bool buttonKey, ref bool triggerKey)
    {
        if (buttonKey == triggerKey)
        {
            triggerKey = !triggerKey;
            return buttonKey;
        }
        else
        {
            return false;
        }
    }

    protected virtual void DeactivateController()
    {
        enabled = false;
    }

    public void ActivateController(StateInputController state)
    {
        for (int i = 0; i < AllInputControllers.Count; i++)
        {
            if (AllInputControllers[i].stateController == state)
            {
                AllInputControllers[i].enabled = true;
                currentInputControllers = AllInputControllers[i];
            }
            else
            {
                AllInputControllers[i].DeactivateController();
            }
        }
    }

    public static InputController GetInputController(StateInputController state)
    {
        for (int i = 0; i < AllInputControllers.Count; i++)
        {
            if (AllInputControllers[i].stateController == state)
            {
                return AllInputControllers[i];
            }
        }
        Debug.LogError("Controller not found");
        return null;
    }
}
