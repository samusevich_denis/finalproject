﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GameController : Controller
{
    [SerializeField] private GameObject[] gameObjectsNumber;

    public static int CountEnemy 
    {
        get => countEnemy;
        set 
        {
            countEnemy = value;
            if (countEnemy == 0)
            {
                Controller.WaveComplete();
            }
        } 
    }
    private static int countEnemy;
    public static GameController Controller { get; private set; }
    public static Transform Camera { get; private set; }
    public static UIManager UIManager { get; private set; }
    public static Transform Hero { get; private set; }
    public static LevelGenerator LevelGenerator { get; private set; }
    public static PortalGeneratorEnemy PortalGenerator { get; private set; }
  

    private DataLevel dataLevel;
    private static int countWave;

    public override void InitScene()
    {
        Controller = this;
        countWave = 0;
        countEnemy = 0;
        Camera = Instantiate(SettingPrefabs.GamePrefabs.Camera).transform;
        Instantiate(SettingPrefabs.GamePrefabs.EventSystem);
        var uiManager = Instantiate(SettingPrefabs.GamePrefabs.UIGamePanel);
        UIManager = uiManager.GetComponent<UIManager>();
        UIManager.Activation();
        Hero = Instantiate(SettingPrefabs.GamePrefabs.Hero).transform;
        var objLevel =  Instantiate(SettingPrefabs.GamePrefabs.LevelGenerator, Vector3.zero, Quaternion.identity).transform;
        LevelGenerator = objLevel.GetComponent<LevelGenerator>();
        var objPortal = Instantiate(SettingPrefabs.GamePrefabs.PortalGenerator, Vector3.zero, Quaternion.identity).transform;
        PortalGenerator = objPortal.GetComponent<PortalGeneratorEnemy>();
        Instantiate(SettingPrefabs.GamePrefabs.MenuPause, Vector3.zero, Quaternion.identity);
        dataLevel = SettingPrefabs.GamePrefabs.DataLevel;
    }

    public override void Deactivation()
    {
        Cursor.visible = true;
    }

    public override void Activation()
    {
        Cursor.visible = false;
        Time.timeScale = 0;
        InputController.currentInputControllers.ActivateController(StateInputController.Game);
        StartCoroutine(StartWaveNumber(countWave));
    }
    
    private IEnumerator StartWaveNumber(int numberWave)
    {
        yield return StartCoroutine(UIManager.MoveTitle(numberWave));
        Time.timeScale = 1;
        PortalGenerator.SpawnPortal(dataLevel.dataWaves[numberWave]);
    }

    private IEnumerator StartGameOver()
    {
        yield return StartCoroutine(UIManager.MoveTitleGameOver());
        Time.timeScale = 0;
        SceneLoader.GetSceneLoader().LoadScene(StateGame.MainMenu);
    }
    private IEnumerator StartWinPlayer()
    {
        yield return StartCoroutine(UIManager.MoveTitleWinPlayer());
        Time.timeScale = 0;
        SceneLoader.GetSceneLoader().LoadScene(StateGame.MainMenu);
    }
    public void GameOver()
    {
        StartCoroutine(StartGameOver());
    }
    private void HeroWin()
    {
        StartCoroutine(StartWinPlayer());
    }

    private void WaveComplete()
    {

        countWave++;
        if (countWave == 10)
        {
            HeroWin();
            return;
        }
        StartCoroutine(StartWaveNumber(countWave));
    }
}
