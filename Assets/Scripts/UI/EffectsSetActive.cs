﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsSetActive : MonoBehaviour
{
    [SerializeField] EffectTitle[] effectTitles;

    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
        for (int i = 0; i < effectTitles.Length; i++)
        {
            effectTitles[i].SetActive(isActive);
        }
    }
}
