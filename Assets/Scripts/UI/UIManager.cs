﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private TitleMove titleWave;
    [SerializeField] private TitleMove titleGameOver;
    [SerializeField] private TitleMove titleWinPlayer;
    [SerializeField] private GameObject[] gameObjectsNumber;
    [SerializeField] private Image imageHealth;
    [SerializeField] private Image imageSkill;
    [SerializeField] private Image imageAmmo;
    [SerializeField] private UIWeapon[] weapon;
    [SerializeField] private Text gold;

    private static Image ImageHealth { get; set; }
    private static Image ImageSkill { get; set; }
    private static Image ImageAmmo { get; set; }
    private static UIWeapon[] Weapon { get; set; }
    private static Text Gold { get; set; }

    public void Activation()
    {
        ImageHealth = imageHealth;
        ImageSkill = imageSkill;
        ImageAmmo = imageAmmo;
        Weapon = weapon;
        for (int i = 0; i < Weapon.Length; i++)
        {
            Weapon[i].SpriteDeactive();
        }
        Gold = gold;
    }

    public static void SetGoldForUI(int coin)
    {
        Gold.text = "Gold " + coin.ToString("D6");
    }

    public static void SetHealthForUI(int health)
    {
        ImageHealth.fillAmount = (float)health / (float)Settings.Hero.MaxHealth;
    }

    public static void SetSkillForUI(float timerSkill)
    {
        ImageSkill.fillAmount = (Settings.Hero.SkillPush.TimeSkillReload - timerSkill) / Settings.Hero.SkillPush.TimeSkillReload;
    }

    public static void SetAmmoForUI()
    {
        ImageAmmo.fillAmount = (float)Inventory.HeroWeapons[Inventory.CurrentIndexWeapon].countBullet / (float)Inventory.HeroWeapons[Inventory.CurrentIndexWeapon].quantityBullets;
    }
    public static void SetRelloadForUI(float time)
    {
        ImageAmmo.fillAmount = (Inventory.HeroWeapons[Inventory.CurrentIndexWeapon].reloadTime - time )/ Inventory.HeroWeapons[Inventory.CurrentIndexWeapon].reloadTime;
    }
    public static void SetWeaponForUI(int indexWeapon)
    {
        Weapon[indexWeapon].SetSprite(Inventory.HeroWeapons[indexWeapon]);
    }

    public IEnumerator MoveTitle(int numberWave)
    {
        for (int i = 0; i < gameObjectsNumber.Length; i++)
        {
            if (i == numberWave)
            {
                gameObjectsNumber[i].SetActive(true);
            }
            else
            {
                gameObjectsNumber[i].SetActive(false);
            }
        }
        yield return StartCoroutine(titleWave.MoveTitle());
    }

    public IEnumerator MoveTitleGameOver()
    {
        yield return StartCoroutine(titleGameOver.MoveTitle());
    }
    public IEnumerator MoveTitleWinPlayer()
    {
        yield return StartCoroutine(titleWinPlayer.MoveTitle());
    }
    
}
