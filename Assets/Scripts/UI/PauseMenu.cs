﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    private bool triggerPause;

    private void Start()
    {
        gameObject.SetActive(false);
        InputControllerGame.PauseAction += PauseGameAction;
    }

    public void ExitGame()
    {
        SceneLoader.GetSceneLoader().LoadScene(StateGame.MainMenu);
    }

    public void PauseGameAction()
    {
        triggerPause = !triggerPause;
        Time.timeScale = triggerPause ? 0 : 1;
        Cursor.visible = triggerPause;
        gameObject.SetActive(triggerPause);
        if (triggerPause)
        {
            StartCoroutine(InputPause());
        }
    }

    private IEnumerator InputPause()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(Time.fixedDeltaTime);
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                PauseGameAction();
                break;
            }
        }
    }

    private void OnDestroy()
    {
        InputControllerGame.PauseAction -= PauseGameAction;
    }
}
