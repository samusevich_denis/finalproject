﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectTitle : MonoBehaviour
{
    [SerializeField] private RectTransform right;
    [SerializeField] private RectTransform left;

    private ParticleSystem particleSystemTitle;
    private bool directionTrigger;
    private Vector3 direction;
    private float currentPosition;
    private float speed = Settings.UI.Menu.SpeedMoveEffectTitle;
    private bool isMove;

    void Start()
    {
        particleSystemTitle = GetComponent<ParticleSystem>();
        directionTrigger = true;
        direction = transform.right;
    }

    void Update()
    {
        if (!isMove)
        {
            EffectSetActive(true);
            return;
        }
        direction = directionTrigger ? transform.right : transform.right *- 1;
        currentPosition = directionTrigger ? right.position.x : left.position.x;
        if (Mathf.Abs(transform.position.x - currentPosition) < 0.1)
        {
            directionTrigger = !directionTrigger;
        }
        transform.Translate(direction * Time.deltaTime* speed);
        EffectSetActive(false);
    }

    private void EffectSetActive(bool isActive)
    {
        if (right.gameObject.activeSelf == isActive)
        {
            isMove = isActive;
            if (isActive)
                particleSystemTitle.Play();
            else
                particleSystemTitle.Stop();
        }
    }

    public void SetActive(bool isActive)
    {
        if (isActive)
            particleSystemTitle.Play();
        else
            particleSystemTitle.Stop();
    }
}
