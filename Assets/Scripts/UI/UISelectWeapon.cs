﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISelectWeapon : MonoBehaviour
{
    private RectTransform rectTransform;
    private float speedMove = Settings.UI.Game.SpeedMoveSelectWeapon;
    private float stepPivot = Settings.UI.Game.StepPivotSelectWeapon;

    public static bool IsWeaponChoice { get; set; }

    private void Start()
    {
        IsWeaponChoice = false;
        rectTransform = GetComponent<RectTransform>();
    }

    private void Update()
    {
        if (!IsWeaponChoice)
        {
            return;
        }
        if (Mathf.Abs(rectTransform.pivot.x- (stepPivot+ stepPivot*Inventory.CurrentIndexWeapon))<0.02)
        {
            var pivot = rectTransform.pivot;
            pivot.x = stepPivot + stepPivot * Inventory.CurrentIndexWeapon;
            rectTransform.pivot= pivot;
            IsWeaponChoice = false;
        }
        var pivotMove = rectTransform.pivot;
        pivotMove.x = Mathf.Lerp(pivotMove.x, stepPivot + stepPivot * Inventory.CurrentIndexWeapon, Time.deltaTime * speedMove);
        rectTransform.pivot = pivotMove;
    }
}
