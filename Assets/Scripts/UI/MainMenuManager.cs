﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] private RectTransform hero;
    [SerializeField] private RectTransform title;
    [SerializeField] private RectTransform menu;
    private float speedTranslate = Settings.UI.Menu.SpeedTranslateHeroForTitle;
    private float appearanceDelayMenu = Settings.UI.Menu.AppearanceDelayMenu;
    private float appearanceDelayTitle = Settings.UI.Menu.AppearanceDelayTitle;
    private float maxWay = Settings.UI.Menu.MaxWayHeroForTitle;

    public void Start()
    {
        for (int i = 0; i < title.childCount; i++)
        {
            title.GetChild(i).gameObject.SetActive(false);
        }
        hero.gameObject.SetActive(false);
        menu.gameObject.SetActive(false);
    }
    public IEnumerator InitMenu()
    {
        yield return StartCoroutine(StartActiveTitle()); ;
    }
    private IEnumerator StartActiveTitle()
    {
        hero.gameObject.SetActive(true);
        int indexLetter = 0;
        while (hero.anchoredPosition.x < maxWay || indexLetter <= title.childCount - 1)
        {
            hero.Translate(hero.right * Time.deltaTime * speedTranslate);
            if (indexLetter < title.childCount && title.GetChild(indexLetter).transform.position.x + appearanceDelayTitle < hero.position.x)
            {
                title.GetChild(indexLetter).gameObject.SetActive(true);
                indexLetter++;
            }
            yield return null;
        }
        var pos = hero.transform.position;
        pos.y -= (menu.GetChild(0).position.y - menu.GetChild(1).position.y) * 0.5f + (pos.y - menu.GetChild(0).position.y);
        hero.transform.position = pos;
        hero.transform.rotation = Quaternion.Euler(0f, Constants.AngleForAxisY.DirectionalLeft, 0f);
        yield return new WaitForSeconds(1f);
        while (hero.anchoredPosition.x > -maxWay)
        {
            hero.Translate(hero.right * -1 * Time.deltaTime * speedTranslate);
            if (menu.transform.position.x - appearanceDelayMenu > hero.position.x)
            {
                menu.gameObject.SetActive(true);
                indexLetter++;
            }
            yield return null;
        }
        hero.gameObject.SetActive(false);
    }

    public  void DeactivationMenu()
    {
        for (int i = 0; i < title.childCount; i++)
        {
            title.GetChild(i).gameObject.SetActive(false);
        }
        hero.gameObject.SetActive(false);
        menu.gameObject.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        SceneLoader.GetSceneLoader().LoadScene(StateGame.Game);
    }
}
