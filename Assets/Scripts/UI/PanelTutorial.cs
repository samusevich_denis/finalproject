﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelTutorial : MonoBehaviour
{
    private InputControllerGame inputControllerGame;
    private ButtonPanelTutorial buttonPanelTutorial;

    void Start()
    {

        inputControllerGame = (InputControllerGame)InputController.GetInputController(StateInputController.Game);
        buttonPanelTutorial = GetComponentInChildren<ButtonPanelTutorial>();
        gameObject.SetActive(false);
    }

    public void ActivePanel(bool isActive)
    {
        gameObject.SetActive(isActive);
        if (isActive)
        {
            ShowTutorial();
        }
        else
        {
            StopTutorial();
        }
    }
    private void ShowTutorial()
    {
        inputControllerGame.StartInputTutorial();
        buttonPanelTutorial.StartButtonTutorial();
    }

    private void StopTutorial()
    {
        inputControllerGame.StopInputTutorial();
        buttonPanelTutorial.StopButtonTutorial();
        var controller = (MenuController)MenuController.GetController();
        controller.ReloadRoomTutorial();
    }
}
