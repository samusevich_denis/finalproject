﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonAnimation : MonoBehaviour
{
    [SerializeField] private AnimationCurve animationCurve;
    [SerializeField] private RectTransform selectObjectLeft;
    [SerializeField] private RectTransform selectObjectRight;

    private Vector3 startPositionLeft;
    private Vector3 startPositionRight;
    private Animator animatorButton;
    private bool isButtonSelected;
    private float offsetSelectObject = Settings.UI.Button.OffsetSelectObject;
    private float timeCurve;
    private float TimeCurve
    {
        get
        {
            return timeCurve;
        }
        set
        {
            if (value>1)
            {
                timeCurve = value - 1;
                return;
            }
            timeCurve = value;
        }
    }

    void Start()
    {
        animatorButton = GetComponent<Animator>();
        startPositionLeft = selectObjectLeft.anchoredPosition;
        startPositionRight = selectObjectRight.anchoredPosition;
        isButtonSelected = false;
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    public void SetAnimation(bool isActive)
    {
        isButtonSelected = isActive;
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(isActive);
        }
        if (isButtonSelected)
        {
            StartCoroutine(SelectedButton());
        }
    }

    private IEnumerator SelectedButton()
    {
        while (isButtonSelected)
        {
            var posLeft = selectObjectLeft.anchoredPosition;
            posLeft.x = startPositionLeft.x + animationCurve.Evaluate(TimeCurve) * offsetSelectObject;
            selectObjectLeft.anchoredPosition = posLeft;
            var posRight = selectObjectRight.anchoredPosition;
            posRight.x = startPositionRight.x - animationCurve.Evaluate(TimeCurve) * offsetSelectObject;
            selectObjectRight.anchoredPosition = posRight;
            TimeCurve += Time.fixedDeltaTime;
            yield return new WaitForSeconds(Time.fixedDeltaTime);
        }
    }

    public void ClickButton()
    {
        animatorButton.SetTrigger("Press");
    }
}
