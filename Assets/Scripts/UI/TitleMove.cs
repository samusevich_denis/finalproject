﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleMove : MonoBehaviour
{
    [SerializeField] private AnimationCurve animationCurve;

    private float wayMove = Settings.UI.Game.WayMoveTitle;
    private float speedMove = Settings.UI.Game.SpeedMoveTitle;
    private RectTransform rectTransform;

    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        var pos = rectTransform.anchoredPosition;
        pos.x = wayMove;
        rectTransform.anchoredPosition = pos;
    }

    public IEnumerator MoveTitle()
    {
        float step = 0;
        while (rectTransform.anchoredPosition.x > -wayMove)
        {
            var pos = rectTransform.anchoredPosition;
            pos.x = wayMove*animationCurve.Evaluate(step);
            rectTransform.anchoredPosition = pos;
            step += Time.fixedDeltaTime* speedMove;
            yield return new WaitForSecondsRealtime(Time.fixedDeltaTime);
        }
        var posEnd = rectTransform.anchoredPosition;
        posEnd.x = wayMove;
        rectTransform.anchoredPosition = posEnd;
    }
}
