﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIWeapon : MonoBehaviour
{
    [SerializeField] private GameObject[] gameObjectsEffect;
    [SerializeField] private Image imageWeapon;

    private int indexEffectActive;

    public void SetSprite(DataWeapon weapon)
    {
        var bulletEffect = weapon.bullet.GetComponent<Bullet>();
        SpriteActive((int)bulletEffect.effect);
        imageWeapon.sprite = weapon.spriteWeapon;
        imageWeapon.color = Color.white;
    }

    public void SpriteDeactive()
    {
        var color = imageWeapon.color;
        color.a = 0f;
        imageWeapon.color = color;
        for (int i = 0; i < gameObjectsEffect.Length; i++)
        {
            gameObjectsEffect[i].SetActive(false);
        }
    }

    private void SpriteActive(int index)
    {
        if (indexEffectActive==-1&& index == 0)
        {
            return;
        }
        if (indexEffectActive>-1)
        {
            gameObjectsEffect[indexEffectActive].SetActive(false);
        }
        indexEffectActive = --index;
        if (index>-1)
        {
            gameObjectsEffect[index].SetActive(true);
        }
    }
}
