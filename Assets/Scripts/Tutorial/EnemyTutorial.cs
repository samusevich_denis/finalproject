﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTutorial : Enemy
{
    public override void DieEnemy()
    {
        GameController.CountEnemy -= 1;
        gameObject.layer = Constants.Layer.Effects;
        if (TryGetComponent<EffectEnemy>(out EffectEnemy effectEnemy))
        {
            effectEnemy.StopEffectEnemy();
        }
        enemyAnimation.SetTriggerAnimatorDeath();
        enemyMovement.StopAllCoroutines();
        enemyMovement.StateEnemy = StateEnemyStandart.Die;
        Destroy(gameObject, 1f);
    }
}
