﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonPanelTutorial : MonoBehaviour
{
    [SerializeField] private Image buttonQ;
    [SerializeField] private Image buttonR;
    [SerializeField] private Image buttonA;
    [SerializeField] private Image buttonS;
    [SerializeField] private Image buttonD;
    [SerializeField] private Image buttonLeftShift;
    [SerializeField] private Image buttonSpace;
    [SerializeField] private Image buttonMouse;

    [SerializeField] private GameObject pointerUp;
    [SerializeField] private GameObject pointerDown;
    [SerializeField] private GameObject panelEnd;

    private Coroutine buttonCoroutine;
    private Color colorPressed = new Color(0.5f, 0.5f, 0.5f, 1f);
    private Color colorStandartButton = Color.white;
    private float delayTimePress = 0.2f;
    private void Start()
    {
        pointerUp.SetActive(false);
        pointerDown.SetActive(false);
        panelEnd.SetActive(false);
    }

    public void StartButtonTutorial()
    {
        buttonCoroutine = StartCoroutine(InputTutorial());
    }
    public void StopButtonTutorial()
    {
        StopCoroutine(buttonCoroutine);
        pointerUp.SetActive(false);
        pointerDown.SetActive(false);
        panelEnd.SetActive(false);
        buttonQ.color = colorStandartButton;
        buttonR.color = colorStandartButton;
        buttonA.color = colorStandartButton;
        buttonS.color = colorStandartButton;
        buttonD.color = colorStandartButton;
        buttonLeftShift.color = colorStandartButton;
        buttonSpace.color = colorStandartButton;
        buttonMouse.color = colorStandartButton;
    }

    public IEnumerator InputTutorial()
    {
        panelEnd.SetActive(false);
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(MoveTutorial(buttonA, 1f, 1f));
        yield return StartCoroutine(MoveTutorial(buttonD, 2f, 1f));
        yield return StartCoroutine(MoveTutorial(buttonA, 2f, 1f));
        yield return StartCoroutine(CrouchDownTutorial(buttonS, 1.5f, 1f));
        yield return StartCoroutine(CrouchDownTutorial(buttonS, 1.5f, 1f));
        yield return StartCoroutine(JumpTutorial(buttonSpace, 3f));
        yield return StartCoroutine(JumpTutorial(buttonSpace, 3f));
        yield return StartCoroutine(JumpFreezeTutorial(buttonSpace, 0.8f, 2f));
        buttonSpace.color = colorPressed;
        yield return new WaitForFixedUpdate();
        yield return new WaitForSeconds(delayTimePress);
        buttonSpace.color = colorStandartButton;
        yield return new WaitForSeconds(2f - delayTimePress);
        yield return StartCoroutine(JumpFreezeTutorial(buttonSpace, 0.8f, 2f));
        buttonS.color = colorPressed;
        yield return new WaitForFixedUpdate();
        yield return new WaitForSeconds(delayTimePress);
        buttonS.color = colorStandartButton;
        yield return new WaitForSeconds(2f - delayTimePress);
        buttonMouse.color = colorPressed;
        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(MoveTutorial(buttonA, 1f, 1f));
        yield return StartCoroutine(MoveTutorial(buttonD, 2f, 1f));
        yield return StartCoroutine(MoveTutorial(buttonA, 2f, 1f));
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(CrouchDownTutorial(buttonS, 1.5f, 1f));
        yield return StartCoroutine(JumpTutorial(buttonSpace, 3f));
        yield return StartCoroutine(JumpTutorial(buttonSpace, 3f));
        yield return StartCoroutine(JumpFreezeTutorial(buttonSpace, 0.8f, 2f));
        buttonSpace.color = colorPressed;
        yield return new WaitForFixedUpdate();
        buttonSpace.color = colorStandartButton;
        yield return new WaitForSeconds(2f);
        buttonMouse.color = colorStandartButton;
        yield return new WaitForSeconds(3f);
        yield return StartCoroutine(PushTutorial(buttonLeftShift, 3f));
        yield return new WaitForSeconds(1.5f);
        pointerUp.gameObject.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        yield return StartCoroutine(PushTutorial(buttonLeftShift, 3f));
        pointerUp.gameObject.SetActive(false);
        yield return new WaitForSeconds(1f);
        pointerDown.gameObject.SetActive(true);
        yield return StartCoroutine(MoveTutorial(buttonD, 2f, 1f));
        yield return new WaitForSeconds(1f);
        buttonMouse.color = colorPressed;
        yield return new WaitForSeconds(2f);
        buttonMouse.color = colorStandartButton;
        yield return StartCoroutine(NextWeaponTutorial(buttonQ, 1f));
        yield return StartCoroutine(NextWeaponTutorial(buttonQ, 1f));
        buttonMouse.color = colorPressed;
        yield return new WaitForSeconds(2f);
        buttonMouse.color = colorStandartButton;
        yield return StartCoroutine(NextWeaponTutorial(buttonQ, 2f));
        buttonMouse.color = colorPressed;
        yield return new WaitForSeconds(2f);
        buttonMouse.color = colorStandartButton;
        yield return StartCoroutine(NextWeaponTutorial(buttonQ, 1f));
        yield return StartCoroutine(NextWeaponTutorial(buttonQ, 1f));
        pointerUp.gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(ReloadTutorial(buttonR, 2f));
        yield return StartCoroutine(NextWeaponTutorial(buttonQ, 2f));
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(ReloadTutorial(buttonR, 2f));
        pointerUp.gameObject.SetActive(false);
        pointerDown.gameObject.SetActive(false);
        panelEnd.SetActive(true);
    }
    private IEnumerator MoveTutorial(Image button, params float[] time)
    {
        button.color = colorPressed;
        yield return new WaitForSeconds(time[0]);
        button.color = colorStandartButton;
        yield return new WaitForSeconds(time[1]);
    }
    private IEnumerator CrouchDownTutorial(Image button, params float[] time)
    {
        button.color = colorPressed;
        yield return new WaitForSeconds(time[0]);
        button.color = colorStandartButton;
        yield return new WaitForSeconds(time[1]);
    }
    private IEnumerator JumpTutorial(Image button, float time)
    {
        button.color = colorPressed;
        yield return new WaitForFixedUpdate();
        yield return new WaitForSeconds(delayTimePress);
        button.color = colorStandartButton;
        yield return new WaitForSeconds(time- delayTimePress);
    }
    private IEnumerator JumpFreezeTutorial(Image button, params float[] time)
    {
        button.color = colorPressed;
        yield return new WaitForFixedUpdate();
        yield return new WaitForSeconds(delayTimePress);
        button.color = colorStandartButton;
        yield return new WaitForSeconds(time[0]- delayTimePress);
        button.color = colorPressed;
        yield return new WaitForFixedUpdate();
        yield return new WaitForSeconds(delayTimePress);
        button.color = colorStandartButton;
        yield return new WaitForSeconds(time[1]-delayTimePress);
    }
    private IEnumerator PushTutorial(Image button,float time)
    {
        button.color = colorPressed;
        yield return new WaitForFixedUpdate();
        yield return new WaitForSeconds(delayTimePress);
        button.color = colorStandartButton;
        yield return new WaitForSeconds(time- delayTimePress);
    }
    private IEnumerator NextWeaponTutorial(Image button, float time)
    {
        button.color = colorPressed;
        yield return new WaitForFixedUpdate();
        yield return new WaitForSeconds(delayTimePress);
        button.color = colorStandartButton;
        yield return new WaitForSeconds(time- delayTimePress);
    }
    private IEnumerator ReloadTutorial(Image button, float time)
    {
        button.color = colorPressed;
        yield return new WaitForFixedUpdate();
        yield return new WaitForSeconds(delayTimePress);
        button.color = colorStandartButton;
        yield return new WaitForSeconds(time- delayTimePress);
    }
}

