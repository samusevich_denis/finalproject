﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTutorial : CameraMovement
{

    void Update()
    {
        var positionMouseToWorld = InputControllerGame.MousePosition + MenuController.Hero.position;
        crosshairs.position = positionMouseToWorld;
        var playerPosition = MenuController.Hero.position;
        playerPosition.z = transform.position.z;
        transform.position = Vector3.Lerp(transform.position, playerPosition, 0.4f);
    }
}
