﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingPrefabs : MonoBehaviour
{
    
    public static class MenuPrefabs
    {
        public static GameObject Camera { get; set; }
        public static GameObject CameraTutorial { get; set; }
        public static GameObject RoomTutorial { get; set; }
        public static GameObject Menu { get; set; }
        public static GameObject EventSystem { get; set; }
    }
    public static class GamePrefabs
    {
        public static GameObject Camera { get; set; }
        public static GameObject MenuPause { get; set; }
        public static GameObject Hero { get; set; }
        public static GameObject LevelGenerator { get; set; }
        public static GameObject PortalGenerator { get; set; }
        public static GameObject Portal { get; set; }
        public static GameObject Room { get; set; }
        public static GameObject PointsPortal { get; set; }
        public static GameObject UIGamePanel { get; set; }
        public static GameObject EventSystem { get; set; }
        public static DataLevel DataLevel { get; set; }
    }

    public static class EffectEnemy
    {
        public static GameObject Fire { get;  set; }
        public static GameObject Lightning { get;  set; }
        public static GameObject AcidCloud { get;  set; }
    }
 
    public static class Bonus
    {
        public static GameObject StandartBonusPrefab { get; set; }
        public static class GemsSprite
        {

            public static Sprite Coin { get; set; }
            public static Sprite DiamondSmall { get; set; }
            public static Sprite DiamondMedium { get; set; }
            public static Sprite DiamondBig { get; set; }
            public static Sprite TopazSmall { get; set; }
            public static Sprite TopazMedium { get; set; }
            public static Sprite TopazBig { get; set; }
            public static Sprite EmeraldSmall { get; set; }
            public static Sprite EmeraldMedium { get; set; }
            public static Sprite EmeraldBig { get; set; }
            public static Sprite RubySmall { get; set; }
            public static Sprite RubyMedium { get; set; }
            public static Sprite RubyBig { get; set; }
        }
        public static class Hearts
        {
            public static class Sprites
            {
                public static Sprite Small { get; set; }
                public static Sprite Medium { get; set; }
                public static Sprite Big { get; set; }
            }
        }

        public static class Weapon
        {
            public static DataWeapon StandartHeroWeapon { get; set; }
            public static DataWeapon StandartEnemyWeapon { get; set; }
            public static DataWeapon[] DataWeapons { get; set; }
        }

    }

    [SerializeField] private GameObject menuCamera;
    [SerializeField] private GameObject cameraTutorial;
    [SerializeField] private GameObject roomTutorial;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject eventSystem;

    [SerializeField] private GameObject gameCamera;
    [SerializeField] private GameObject menuPause;
    [SerializeField] private GameObject hero;
    [SerializeField] private GameObject levelGenerator;
    [SerializeField] private GameObject portalGenerator;
    [SerializeField] private GameObject portal;
    [SerializeField] private GameObject prefabRoom;
    [SerializeField] private GameObject prefabPointsPortal;
    [SerializeField] private GameObject uiGamePanel;
    [SerializeField] private DataLevel dataLevel;

    [SerializeField] private GameObject fire;
    [SerializeField] private GameObject effectLightning;
    [SerializeField] private GameObject acidCloud;

    [SerializeField] private GameObject standartBonusPrefab;
    [SerializeField] private Sprite coin;
    [SerializeField] private Sprite diamondSmall;
    [SerializeField] private Sprite diamondMedium;
    [SerializeField] private Sprite diamondBig;
    [SerializeField] private Sprite topazSmall;
    [SerializeField] private Sprite topazMedium;
    [SerializeField] private Sprite topazBig;
    [SerializeField] private Sprite emeraldSmall;
    [SerializeField] private Sprite emeraldMedium;
    [SerializeField] private Sprite emeraldBig;
    [SerializeField] private Sprite rubySmall;
    [SerializeField] private Sprite rubyMedium;
    [SerializeField] private Sprite rubyBig;


    [SerializeField] private Sprite heartSmall;
    [SerializeField] private Sprite heartMedium;
    [SerializeField] private Sprite heartBig;

    [SerializeField] private DataWeapon standartHeroWeapon;
    [SerializeField] private DataWeapon standartEnemyWeapon;
    [SerializeField] private DataWeapon[] dataWeapons;

    private void Awake()
    {
        MenuPrefabs.Camera = menuCamera;
        MenuPrefabs.CameraTutorial = cameraTutorial;
        MenuPrefabs.RoomTutorial = roomTutorial;
        MenuPrefabs.Menu = mainMenu;
        MenuPrefabs.EventSystem = eventSystem;

        GamePrefabs.Camera = gameCamera;
        GamePrefabs.MenuPause = menuPause;
        GamePrefabs.Hero = hero;
        GamePrefabs.LevelGenerator = levelGenerator;
        GamePrefabs.PortalGenerator = portalGenerator;
        GamePrefabs.Portal = portal;
        GamePrefabs.UIGamePanel = uiGamePanel;
        GamePrefabs.Room =  prefabRoom;
        GamePrefabs.PointsPortal = prefabPointsPortal;
        GamePrefabs.EventSystem = eventSystem;
        GamePrefabs.DataLevel = dataLevel;

        EffectEnemy.Fire = fire;
        EffectEnemy.Lightning = effectLightning;
        EffectEnemy.AcidCloud = acidCloud;

        Bonus.StandartBonusPrefab = standartBonusPrefab;
        Bonus.GemsSprite.Coin = coin;
        Bonus.GemsSprite.DiamondSmall = diamondSmall;
        Bonus.GemsSprite.DiamondMedium = diamondMedium;
        Bonus.GemsSprite.DiamondBig = diamondBig;
        Bonus.GemsSprite.TopazSmall = topazSmall;
        Bonus.GemsSprite.TopazMedium = topazMedium;
        Bonus.GemsSprite.TopazBig = topazBig;
        Bonus.GemsSprite.EmeraldSmall = emeraldSmall;
        Bonus.GemsSprite.EmeraldMedium = emeraldMedium;
        Bonus.GemsSprite.EmeraldBig = emeraldBig;
        Bonus.GemsSprite.RubySmall = rubySmall;
        Bonus.GemsSprite.RubyMedium = rubyMedium;
        Bonus.GemsSprite.RubyBig = rubyBig;

        Bonus.Hearts.Sprites.Small = heartSmall;
        Bonus.Hearts.Sprites.Medium = heartMedium;
        Bonus.Hearts.Sprites.Big = heartBig;

        Bonus.Weapon.StandartHeroWeapon = DataWeapon.GetDataWeapon(standartHeroWeapon, 0);
        Bonus.Weapon.StandartEnemyWeapon = DataWeapon.GetDataWeapon(standartEnemyWeapon, 0);
        Bonus.Weapon.DataWeapons = new DataWeapon[dataWeapons.Length];
        for (int i = 0; i < dataWeapons.Length; i++)
        {
            Bonus.Weapon.DataWeapons[i] = DataWeapon.GetDataWeapon(dataWeapons[i], 0);
        }
    }
}
