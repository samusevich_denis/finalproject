﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, IHealthEnemy
{
    [SerializeField] private int healthEnemy;
    public DataWeapon DataWeaponEnemy { get; private set; }

    public int HealthEnemy 
    {
        get => healthEnemy;
        set
        {
            if (healthEnemy <= 0) { return; }

            healthEnemy = value;
            if (healthEnemy <= 0)
            {
                healthEnemy = 0;
                DieEnemy();
                return;
            }
            enemyAnimation.SetTriggerAnimatorHurt();
        }
    }

    protected EnemyMovement enemyMovement;
    protected EnemyAnimation enemyAnimation;
    private Rigidbody2D enemyRigidbody2D;

    void Start()
    {
        GameController.CountEnemy += 1;
        enemyRigidbody2D = transform.gameObject.GetComponent<Rigidbody2D>();
        enemyMovement = GetComponent<EnemyMovement>();
        enemyAnimation = GetComponentInChildren<EnemyAnimation>();
    }

    public void SetDataEnemy(DataEnemy enemyData, bool isRotate, float direction)
    {
        var sprite = GetComponentInChildren<SpriteRenderer>();
        sprite.color = enemyData.colorEnemy;
        transform.localScale *= enemyData.sizeEnemy;
        healthEnemy = enemyData.baseHealthEnemy;
        DataWeaponEnemy =  DataWeapon.GetDataWeapon(enemyData.dataWeapon,0);
        var rotate = GetComponentInChildren<EnemyRotate>();
        rotate.StartRotateEnemy(isRotate, direction);
    }

    public virtual void DieEnemy()
    {
        GameController.CountEnemy -= 1;
        gameObject.layer = Constants.Layer.IgnoreRaycast;
        if (TryGetComponent<EffectEnemy>(out EffectEnemy effectEnemy))
        {
            effectEnemy.StopEffectEnemy();
        }
        enemyAnimation.SetTriggerAnimatorDeath();
        enemyMovement.StopAllCoroutines();
        enemyMovement.StateEnemy = StateEnemyStandart.Die;

        Destroy(gameObject, 1f);
        Bonus.CreateBonus(transform.position);
    }

    public void ToDamageEnemy(Vector2 vectorPush, int damage)
    {
        HealthEnemy -= damage;
        enemyRigidbody2D.AddForce(vectorPush * enemyRigidbody2D.mass, ForceMode2D.Impulse);
    }
}
