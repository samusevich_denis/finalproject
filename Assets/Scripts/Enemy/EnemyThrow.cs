﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyThrow : MonoBehaviour
{
    [SerializeField] private Transform pointShot;
    public bool IsReload { get; private set; }

    private EnemyAnimation animatorEnemy;
    private EnemyMovement enemyMovement;
    private Enemy enemy;
    private Coroutine coroutineShot;
    private bool triggerAttack;
    private bool isShotReload;

    void Start()
    {
        enemy = GetComponent<Enemy>();
        enemyMovement = GetComponent<EnemyMovement>(); ;
        animatorEnemy = GetComponentInChildren<EnemyAnimation>();
        triggerAttack = true;
    }

    void Update()
    {
        if ((enemyMovement.StateEnemy == StateEnemyStandart.Throw) == triggerAttack)
        {
            if (triggerAttack)
            {
                if (isShotReload || IsReload)
                {
                    return;
                }
                triggerAttack = false;
                coroutineShot = StartCoroutine(StartShotWeapon());
            }
            else
            {
                triggerAttack = true;
                StopCoroutine(coroutineShot);
            }
        }
    }

    private IEnumerator StartShotWeapon()
    {
        while (enemyMovement.StateEnemy == StateEnemyStandart.Throw)
        {
            while (isShotReload || IsReload)
            {
                yield return null;
                continue;
            }
            if (enemy.DataWeaponEnemy.countBullet == 0)
            {
                IsReload = true;
                StartCoroutine(TimeReload(enemy.DataWeaponEnemy.reloadTime));
                yield return null;
                continue;
            }
            ShotActiveWeapon();
            isShotReload = true;
            StartCoroutine(TimeShotReload(enemy.DataWeaponEnemy.shotTime));
            yield return null;
        }
    }

    private void ShotActiveWeapon()
    {
        animatorEnemy.SetTriggerAnimatorThrow();
        enemy.DataWeaponEnemy.countBullet--;
        var obj = Instantiate(enemy.DataWeaponEnemy.bullet, pointShot.position, Quaternion.identity);
        var bullet = obj.GetComponent<Bullet>();
        var vectorDirection = GameController.Hero.position - pointShot.position; //pointShot.InverseTransformPoint(GameController.Hero.position);
        vectorDirection.y += Settings.Enemy.AdjustmentPositionHero;
        bullet.SetDataBullet(enemy.DataWeaponEnemy, vectorDirection.normalized);
        bullet.Launch();
    }

    private IEnumerator TimeShotReload(float time)
    {
        yield return new WaitForSeconds(time);
        isShotReload = false;
    }

    private IEnumerator TimeReload(float time)
    {
        enemy.DataWeaponEnemy.countBullet = enemy.DataWeaponEnemy.quantityBullets;
        yield return new WaitForSeconds(time);
        IsReload = false;
    }
}
