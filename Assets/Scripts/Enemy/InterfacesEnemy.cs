﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IHealthEnemy
{
    int HealthEnemy { get; set; }
    void ToDamageEnemy(Vector2 vectorPush, int damage);
    void DieEnemy();
}

public interface IAnimationEnemy
{
    void SetDirectionGraphicsEnemy();
    void SetDataAnimatorEnemy();
}

public interface IInitEnemy
{
    void StartInitEnemy();
    void InitEnemy();
}

public interface IIdleEnemy
{
    void IdleEnemy();
    bool CheckIdleEnemy();
}

public interface IMoveEnemy
{
    float MoveSpeed { get; set; }
    void MoveEnemy();
    bool CheckMoveEnemy();
}

public interface IRunEnemy
{
    float RunSpeed { get; set; }
    void RunEnemy();
    bool CheckRunEnemy();
}

public interface IJumpEnemy
{
    float JumpForce { get; set; }
    void StartJumpEnemy();
    bool CheckJumpEnemy();
}

public interface IThrowEnemy
{
    void StartThrowEnemy();
    void ThrowEnemy();
    bool CheckThrowEnemy();
}

public interface IAttackEnemy
{
    int DamageAttack { get; set; }
    void StartAttackEnemy();
    void AttackEnemy();
    bool CheckAttackEnemy();
}

public interface  IStateMachineEnemy<T>
{
    T StateEnemy { get; set; }
    void CheckStateEnemy();
    void StateTransitionEnemy();
}

