﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum StateEnemyStandart
{
    StartInit,
    Init,
    Idle,
    Move,
    Run,
    StartJump,
    Jump,
    StartAttack,
    Attack,
    StartThrow,
    Throw,
    Wait,
    Die,
}

public class EnemyMovement : MonoBehaviour, IStateMachineEnemy<StateEnemyStandart>, IInitEnemy, IIdleEnemy, IMoveEnemy,IRunEnemy, IJumpEnemy, IAttackEnemy, IThrowEnemy
{
    [SerializeField] protected Transform pointRaycast;
    [SerializeField] private Transform pointCenterEnemy;
    [SerializeField] private Transform pointAirRaycast;
    [SerializeField] private Transform pointJumpRaycast;
    [SerializeField] private Transform pointShot;

    public StateEnemyStandart StateEnemy { get; set; }
    public float JumpForce { get; set; } = Settings.Enemy.JumpForce;
    public float MoveSpeed { get; set; } = Settings.Enemy.MoveSpeed;
    public float RunSpeed { get; set; } = Settings.Enemy.RunSpeed;
    public int DamageAttack { get; set; } 
    public Vector3 PositionPlayer { get; private set; }
    public Vector2 Direction { get => direction; private set => direction = value; }

    

    private EnemyThrow enemyThrow;
    private EnemyAnimation animatorEnemy;
    private EnemyRotate enemyRotate;
    protected Rigidbody2D enemyRigidbody2D;
    protected Vector2 direction;
    private bool triggerIdle;
    private bool isTimeIdle;
    private float maxSpeed;

    private void Start()
    {
        DamageAttack = (int)(Settings.Enemy.DamageMeleeAttack * transform.localScale.x);
        maxSpeed = Settings.Enemy.MaxSpeed + transform.localScale.x;
        enemyRigidbody2D = GetComponent<Rigidbody2D>();
        animatorEnemy = GetComponentInChildren<EnemyAnimation>();
        enemyThrow = GetComponent<EnemyThrow>();
        enemyRotate = GetComponentInChildren<EnemyRotate>();
        direction = Random.Range(0,2)>0?Vector2.right:Vector2.left;
        StateEnemy = StateEnemyStandart.StartInit;
    }

    private void Update()
    {
        PositionPlayer = GameController.Hero.position - pointCenterEnemy.position;
    }

    private void FixedUpdate()
    {
        ControlVelocity();
        CheckStateEnemy();
    }

    private void ControlVelocity()
    {
        Vector2 velocity = new Vector2(Mathf.Clamp(enemyRigidbody2D.velocity.x, -maxSpeed, maxSpeed), Mathf.Clamp(enemyRigidbody2D.velocity.y, -maxSpeed, maxSpeed));
        enemyRigidbody2D.velocity = velocity;
    }

    public void CheckStateEnemy()
    {
        SetDirectionMove();
        switch (StateEnemy)
        {
            case StateEnemyStandart.StartInit:
                StartInitEnemy();
                break;
            case StateEnemyStandart.Init:
                InitEnemy();
                break;
            case StateEnemyStandart.Idle:
                IdleEnemy();
                StateTransitionEnemy();
                break;
            case StateEnemyStandart.Move:
                MoveEnemy();
                StateTransitionEnemy();
                break;
            case StateEnemyStandart.Run:
                RunEnemy();
                StateTransitionEnemy();
                break;
            case StateEnemyStandart.StartJump:
                StartJumpEnemy();
                break;
            case StateEnemyStandart.Jump:
                break;
            case StateEnemyStandart.StartAttack:
                StartAttackEnemy();
                break;
            case StateEnemyStandart.Attack:
                AttackEnemy();
                break;
            case StateEnemyStandart.StartThrow:
                StartThrowEnemy();
                break;
            case StateEnemyStandart.Throw:
                ThrowEnemy();
                break;
            case StateEnemyStandart.Wait:
                StateTransitionEnemy();
                break;
            case StateEnemyStandart.Die:
                DieEnemy();
                break;
        }
    }

    private  void SetDirectionMove()
    {
        var hit = Physics2D.Raycast(pointRaycast.position, pointRaycast.right, 0.3f);
        if (hit.transform != null)
        {
            if (hit.transform.TryGetComponent<TilemapCollider2D>(out TilemapCollider2D tilemapCollider2D))
            {
                direction *= -1;
            }
        }
    }

    public void StartInitEnemy()
    {
        StateEnemy = StateEnemyStandart.Init;
    }
    public void InitEnemy()
    {
        if (enemyRotate.enabled) { return; }

        StateEnemy = StateEnemyStandart.Wait;
    }

    public  void IdleEnemy()
    {
        StopMove();
    }

    public  void MoveEnemy()
    {
        Vector2 velocity = enemyRigidbody2D.velocity;
        velocity.x = Mathf.Lerp(velocity.x, direction.x * MoveSpeed, 0.2f);
        enemyRigidbody2D.velocity = velocity;
    }

    public void RunEnemy()
    {
        Vector2 velocity = enemyRigidbody2D.velocity;
        velocity.x = Mathf.Lerp(velocity.x, direction.x * RunSpeed, 0.2f);
        enemyRigidbody2D.velocity = velocity;
    }

    public void StartJumpEnemy()
    {
        enemyRigidbody2D.velocity = Vector2.zero;
        StateEnemy = StateEnemyStandart.Jump;
        animatorEnemy.SetTriggerAnimatorJump();
        StartCoroutine(JumpAndCheckMove());
    }
    private IEnumerator JumpAndCheckMove()
    {
        yield return new WaitForSeconds(0.18f);
        Vector2 vectorJump = pointRaycast.right;
        vectorJump.y = 1f;
        enemyRigidbody2D.AddForce(vectorJump * JumpForce * enemyRigidbody2D.mass, ForceMode2D.Impulse);
        yield return new WaitForSeconds(0.1f);
        while (!CheckMoveEnemy())
        {
            yield return null;
        }
        animatorEnemy.SetTriggerAnimatorLanding();
        direction = Random.Range(0, 2) > 0 ? Vector2.right : Vector2.left;
        StateEnemy = StateEnemyStandart.Move;
    }

    public void StartAttackEnemy()
    {
        StateEnemy = StateEnemyStandart.Attack;
        animatorEnemy.SetTriggerAnimatorAttack();
        DamageHero();
        StartCoroutine(ChangeState(1f));
    }
    private void DamageHero()
    {
        var hit = Physics2D.Raycast(pointRaycast.position, pointRaycast.right, 0.16f);
        if (hit.transform != null && hit.transform.TryGetComponent<IHealthHero>(out IHealthHero healthHero))
        {
            healthHero.ToDamageHero(PositionPlayer.normalized, DamageAttack);
        }
    }
    private IEnumerator ChangeState(float time)
    {
        yield return new WaitForSeconds(time);
        StateEnemy = StateEnemyStandart.Move;
    }
    public void AttackEnemy()
    {
        StopMove();
    }

    public void StartThrowEnemy()
    {
        StateEnemy = StateEnemyStandart.Throw;
        StartCoroutine(CheckThrow());
    }
    private IEnumerator CheckThrow()
    {
        while (CheckThrowEnemy())
        {
            yield return null;
        }
        StateEnemy = StateEnemyStandart.Move;
    }
    public void ThrowEnemy()
    {
        StopMove();
    }

    public void DieEnemy()
    {
        StopMove();
    }
    private void StopMove()
    {
        Vector2 velocity = enemyRigidbody2D.velocity;
        velocity.x = Mathf.Lerp(velocity.x, 0f, 0.2f);
        enemyRigidbody2D.velocity = velocity;
    }

    public  void StateTransitionEnemy()
    {
        if (CheckAttackEnemy())
        {
            StateEnemy = StateEnemyStandart.StartAttack;
            return;
        }
        if (CheckThrowEnemy())
        {
            StateEnemy = StateEnemyStandart.StartThrow;
            return;
        }
        if (CheckJumpEnemy())
        {
            StateEnemy = StateEnemyStandart.StartJump;
            return;
        }
        if (CheckRunEnemy())
        {
            StateEnemy = StateEnemyStandart.Run;
            return;
        }
        if (CheckIdleEnemy())
        {
            StateEnemy = StateEnemyStandart.Idle;
            return;
        }

        if (CheckMoveEnemy())
        {
            StateEnemy = StateEnemyStandart.Move;
            return;
        }
    }

    public  bool CheckAttackEnemy()
    {
        var hit = Physics2D.Raycast(pointRaycast.position, pointRaycast.right, 0.16f);
        if (hit.transform != null&& hit.transform.TryGetComponent<Hero>(out Hero heroPlayer))
        {
            return true;
        }
        return false;
    }
    
    public bool CheckThrowEnemy()
    {
        if (enemyThrow.IsReload || !(StateEnemy == StateEnemyStandart.Move || StateEnemy == StateEnemyStandart.Run || StateEnemy == StateEnemyStandart.Idle || StateEnemy == StateEnemyStandart.Throw))
        {
            return false;
        }
        if (PositionPlayer.y > -0.5f && PositionPlayer.y < 4f 
            && (direction.x >0?
            (PositionPlayer.x > 0 && PositionPlayer.x < 10): 
            (PositionPlayer.x < 0 && PositionPlayer.x > -10)))
        {
            var pointRay = pointShot.position;
            pointRay.y += Settings.Enemy.AdjustmentRaycastToThrow;
            var pointHero = GameController.Hero.position;
            pointHero.y += Settings.Enemy.AdjustmentPositionHero;
            var vectorDirection = pointHero - pointShot.position;
            var hits = Physics2D.RaycastAll(pointRay, vectorDirection, 6f);
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].transform != null 
                    && hits[i].transform.TryGetComponent<TilemapCollider2D>(out TilemapCollider2D tilemapCollider2D) // if(hits[i].transform.TryGetComponent<HeroPlayer>(out HeroPlayer heroPlayer))
                    && tilemapCollider2D.gameObject.layer != Constants.Layer.SmallPlatform)
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public  bool CheckJumpEnemy()
    {
        if (StateEnemy != StateEnemyStandart.Move && StateEnemy != StateEnemyStandart.Run && StateEnemy != StateEnemyStandart.Idle)
        {
            return false;
        }
        var hitPlatform = Physics2D.Raycast(pointJumpRaycast.position, pointJumpRaycast.up * -1f, 0.5f);
        if (hitPlatform.transform == null || !hitPlatform.transform.TryGetComponent<TilemapCollider2D>(out TilemapCollider2D TilemapCollider2D))
        {
            return true;
        }
        return false;
    }

    public  bool CheckRunEnemy()
    {
        if (StateEnemy != StateEnemyStandart.Move && StateEnemy != StateEnemyStandart.Run && StateEnemy != StateEnemyStandart.Idle)
        {
            return false;
        }
        if (PositionPlayer.y > -1.3f && PositionPlayer.y < 2 && (direction.x > 0 ? PositionPlayer.x > 0: PositionPlayer.x < 0))
        {
            return true;
        }
        return false;
    }

    public  bool CheckIdleEnemy()
    {
        if (!isTimeIdle || (StateEnemy != StateEnemyStandart.Move && StateEnemy != StateEnemyStandart.Idle))
        {
            return false;
        }
        if (triggerIdle == isTimeIdle)
        {
            triggerIdle = !triggerIdle;
            StartCoroutine(TriggerIdle(Random.Range(2f, 10f)));
        }
        return true;
    }
    private IEnumerator TriggerIdle(float timer)
    {
        yield return new WaitForSeconds(timer);
        isTimeIdle = !isTimeIdle;
    }

    public  bool CheckMoveEnemy()
    {
        var hits = Physics2D.RaycastAll(pointAirRaycast.position, pointAirRaycast.up * -1f, 0.2f);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].transform != null)
            {
                if (hits[i].transform.TryGetComponent<TilemapCollider2D>(out TilemapCollider2D tilemapCollider2D))
                {
                    return true;
                }
            }
        }
        return false;
    }

    private  void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform != null && collision.transform.TryGetComponent<IHealthHero>(out IHealthHero healthHero))
        {
            healthHero.ToDamageHero(PositionPlayer.normalized, DamageAttack);
        }
        if (StateEnemy == StateEnemyStandart.Init || StateEnemy == StateEnemyStandart.StartInit)
        {
            return;
        }
        if (collision.transform != null && collision.transform.TryGetComponent<Enemy>(out Enemy enemy))
        {
            direction *= -1;
        }
    }
}
