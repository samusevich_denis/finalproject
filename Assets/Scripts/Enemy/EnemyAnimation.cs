﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour, IAnimationEnemy
{
    private EnemyMovement enemyMovement;
    private Animator enemyAnimator;

    private void Start()
    {
        enemyAnimator = GetComponent<Animator>();
        enemyMovement = GetComponentInParent<EnemyMovement>();
    }

    void Update()
    {
        SetDataAnimatorEnemy();
        SetDirectionGraphicsEnemy();
    }

    public void SetDataAnimatorEnemy()
    {
        switch (enemyMovement.StateEnemy)
        {
            case StateEnemyStandart.Move:
                enemyAnimator.SetInteger("Movement", 1);
                break;
            case StateEnemyStandart.Run:
                enemyAnimator.SetInteger("Movement", 2);
                break;
            default:
                enemyAnimator.SetInteger("Movement", 0);
                break;
        }
    }

    public void SetDirectionGraphicsEnemy()
    {
        float direction;
        if (enemyMovement.Direction.x > 0.01)
        {
            direction = Constants.AngleForAxisY.DirectionRight;
        }
        else if (enemyMovement.Direction.x < -0.01)
        {
            direction = Constants.AngleForAxisY.DirectionalLeft;
        }
        else
        {
            return;
        }
        transform.localEulerAngles = new Vector3(0f, direction, 0f);
    }

    public void SetTriggerAnimatorDeath()
    {
        enemyAnimator.SetTrigger("Death");
        enemyAnimator.SetBool("Die", true);
    }

    public void SetTriggerAnimatorHurt()
    {
        enemyAnimator.SetTrigger("Hurt");
    }

    public void SetTriggerAnimatorAttack()
    {
        enemyAnimator.SetTrigger("Attack");
    }

    public void SetTriggerAnimatorJump()
    {
        enemyAnimator.SetTrigger("Jump");
    }

    public void SetTriggerAnimatorLanding()
    {
        enemyAnimator.SetTrigger("Landing");
    }

    public void SetTriggerAnimatorThrow()
    {
        enemyAnimator.SetTrigger("Throw");
    }
}
