﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRotate : MonoBehaviour
{
    private bool isRotate;
    private float speedRotate;
    private CapsuleCollider2D capsuleCollider2D;

    void Start()
    {
        capsuleCollider2D = transform.GetComponent<CapsuleCollider2D>();
    }

    public void StartRotateEnemy(bool isRotate, float direction)
    {
        this.isRotate = isRotate;
        speedRotate = Settings.Enemy.RandomSpeedRotate;
        var rig = transform.parent.GetComponent<Rigidbody2D>();
        Vector2 vectorForce = Settings.Enemy.RandomVectorForce;
        vectorForce.x *= direction;
        var force = Settings.Enemy.RandomForce;
        rig.AddForce(vectorForce* rig.mass* force, ForceMode2D.Impulse);
    }

    void Update()
    {
        if (isRotate)
        {
            transform.Rotate(new Vector3(0f, 0f, speedRotate * Time.deltaTime));
        }
        else
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, 0.05f);
            if (Quaternion.Angle(transform.rotation, Quaternion.identity) < 2)
            {
                transform.rotation = Quaternion.identity;
                capsuleCollider2D.enabled = false;
                this.enabled = false;
            }
        }
    }

    public void SetDataRotate(bool isRotate)
    {
        this.isRotate = isRotate;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision != null&& collision.TryGetComponent<Enemy>(out Enemy enemy))
        {
            return;
        }
        isRotate = false;
    }
}
