﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Settings
{
    public static class UI
    {
        public static float SpeedFadeOutScene { get; } = 10f;
        public static class Menu
        {
            public static float SpeedTranslateHeroForTitle { get; } = 50f;
            public static float AppearanceDelayMenu { get; } = 5f;
            public static float AppearanceDelayTitle { get; } = 5f;
            public static float MaxWayHeroForTitle { get; } = 2000f;
            public static float SpeedMoveEffectTitle { get; } = 3f;
        }

        public static class Game
        {
            public static float SpeedMoveSelectWeapon { get; } = 5f;
            public static float StepPivotSelectWeapon { get; } = 0.5f;
            public static float SpeedMoveTitle { get; } = 1f;
            public static float WayMoveTitle { get; } = 1500f;
        }

        public static class Button
        {
            public static float OffsetSelectObject { get; } = 10f;
        }
    }

    public static class Portal
    {
        public static float SpeedScale { get; } = 2f;
        public static Vector3 StartScale { get; } = new Vector3(0.05f, 0.05f, 1f);
    }
    public static class Enemy
    {
        public static float JumpForce { get;  } = 4;
        public static float MoveSpeed { get;  } = 2;  
        public static float RunSpeed { get;  } = 4; 
        public static int DamageMeleeAttack { get; } = 5;
        public static float MaxSpeed { get; } = 6f;
        public static float AdjustmentPositionHero { get; } = 0.6f;
        public static float AdjustmentRaycastToThrow { get; } = -0.25f;
        public static Vector2 RandomVectorForce { get => new  Vector2(Random.Range(0.7f, 1.3f), Random.Range(0.7f, 1.3f)); }
        public static float RandomForce { get=> Random.Range(4, 8); }
        public static float RandomSpeedRotate { get => Random.Range(180f, 360f); }
    }
    public static class Hero
    {
        public static class Animation
        {
            public static float TimeAnimationHurt { get; } = 0.5f;
        }
        public static int MaxHealth { get; } = 100;

        public static float TimeInvulnerabilityAfterDamage { get; } = 2f;

        public static float SpeedMove { get; } = 5f;

        public static float ForceJump { get; } = 10f;
        public static float AdjustmentMoveAir { get; } = 1f;
        public static float MaxSpeed { get; } = 16f;
        public static float AdjustmentColliderCrouchDown { get; } = 0.7f;
        public static class SkillPush
        {
            public static float PaceSpeedPush { get; } = 0.02f;
            public static float TimeSkillReload { get; } = 10f;
            public static int DamageSuperPush { get; } = 20;
            public static float ForceSuperPushEnemy { get; } = 5f;
            public static Vector2 SizeBoxCastPush { get; } = new Vector2(0.01f, 1.8f);
        }
    }
    public static class Bonus
    {
        public static int RandomBonus
        {
            get
            {
                if (Random.Range(-20, 3)>0)
                {
                    return 0;
                }
                else if (Random.Range(-10, 3) > 0)
                {
                    return 1;
                }
                else
                {
                    return 2;
                }

            }
        }
        public static class Gems
        {
            public static int RandomGems
            {
                get
                {
                    var gem = Random.Range(-20, 13);
                    return gem < 0 ? 0 : gem;
                }
            }
            public static int Coin { get; } = 1;
            public static int DiamondSmall { get; } = 20;
            public static int DiamondMedium { get; } = 40;
            public static int DiamondBig { get; } = 60;
            public static int TopazSmall { get; } = 15;
            public static int TopazMedium { get; } = 35;
            public static int TopazBig { get; } = 55;
            public static int EmeraldSmall { get; } = 10;
            public static int EmeraldMedium { get; } = 30;
            public static int EmeraldBig { get; } = 50;
            public static int RubySmall { get; } = 5;
            public static int RubyMedium { get; } = 25;
            public static int RubyBig { get; } = 45;
        }
        public static class Health
        {
            public static int RandomHealth { get => Random.Range(0, 3); }
            public static int Small { get; } = 25;
            public static int Medium { get; } = 50;
            public static int Big { get; } = 100;
        }
        public static class Weapon
        {
            public static int RandomWeapon { get => Random.Range(0,SettingPrefabs.Bonus.Weapon.DataWeapons.Length); }
        }
    }
    public static class Bullet
    {

    }
    public static class Effects
    {
        public static class Damage
        {
            public static int Fire { get; } = 1;
            public static float SpeedFire { get; } = 0.2f;
            public static int Acid { get; } = 1;
            public static float SpeedAcid { get; } = 1;
            public static int Freeze { get; }
            public static float SpeedFreeze { get; }
            public static int Lightning { get; }
            public static float SpeedLightning { get; }

        }
        public static class ColorEffect
        {
            public static Color Fire { get; } = Color.yellow;
            public static Color Acid { get; } = Color.green;
            public static Color Freeze { get; } = Color.cyan;
            public static Color Lightning { get; } = Color.white;
        }
        public static class Timer
        {
            public static float Fire { get; } = 2f;
            public static float Acid { get; } = 8f;
            public static float Freeze { get; } = 3f;
            public static float Lightning { get; } = 1.5f;
        }

        public static class Additionally
        {
            public static float FireTimerInstantiate { get; } = 1f;
            public static float AcidSpeedMoveCloud { get; } = 1f;
            public static float AcidTimerCloudInstantiate { get; } = 1f;
            public static float FreezeBrakingPower { get; } = 0.5f;
        }

    }

}
