﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class MenuController : Controller
{
    public static Transform Hero { get; private set; }
    public static UIManager UIManager { get; private set; }

    public static MainMenuManager MainMenu;

    private GameObject uiManagerGameobject;
    private GameObject roomTutorial;
    private GameObject cameraTutorial;
    private static Vector3 positionLevelTutorial = new Vector3(-40f, 0f, 0f);
    public override void Activation()
    {
        Time.timeScale = 1;
        InputController.currentInputControllers.ActivateController(StateInputController.None);
        StartCoroutine(MainMenu.InitMenu());
    }

    public override void Deactivation()
    {
        MainMenu.DeactivationMenu();
    }

    public override void InitScene()
    {
        var camera = Instantiate(SettingPrefabs.MenuPrefabs.Camera);
        var objMenu = Instantiate(SettingPrefabs.MenuPrefabs.Menu);
        MainMenu = objMenu.GetComponent<MainMenuManager>();
        var canvas = MainMenu.GetComponent<Canvas>();
        canvas.worldCamera = camera.GetComponent<Camera>();
        Instantiate(SettingPrefabs.GamePrefabs.EventSystem);


        CreateRoomTutorial();
    }
    public static void ReturnHero()
    {
        Hero.position = positionLevelTutorial;
    }

    public void ReloadRoomTutorial()
    {
        Destroy(roomTutorial);
        Destroy(Hero.gameObject);
        Destroy(uiManagerGameobject);
        Destroy(cameraTutorial);
        var objectsEnemy = FindObjectsOfType<Enemy>();
        for (int i = 0; i < objectsEnemy.Length; i++)
        {
            Destroy(objectsEnemy[i].gameObject);
        }
        var objectsBonus = FindObjectsOfType<Bonus>();
        for (int i = 0; i < objectsBonus.Length; i++)
        {
            Destroy(objectsBonus[i].gameObject);
        }
        CreateRoomTutorial();
    }
    private void CreateRoomTutorial()
    {
        roomTutorial = Instantiate(SettingPrefabs.MenuPrefabs.RoomTutorial, positionLevelTutorial, Quaternion.identity, transform);
        Hero = Instantiate(SettingPrefabs.GamePrefabs.Hero, positionLevelTutorial, Quaternion.identity).transform;
        uiManagerGameobject = Instantiate(SettingPrefabs.GamePrefabs.UIGamePanel);
        cameraTutorial = Instantiate(SettingPrefabs.MenuPrefabs.CameraTutorial);
        var canvasUIManager = uiManagerGameobject.GetComponent<Canvas>();
        canvasUIManager.worldCamera = cameraTutorial.transform.GetComponent<Camera>();
        canvasUIManager.renderMode = RenderMode.ScreenSpaceCamera;
        canvasUIManager.sortingLayerID = 1170939161;
        UIManager = uiManagerGameobject.GetComponent<UIManager>();
        UIManager.Activation();
    }
}
