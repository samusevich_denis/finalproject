﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public static int CurrentIndexWeapon { get; private set; } = 0;
    public static int QuantityCoin 
    {
        get => quantityGold;
        private set
        {
            quantityGold = value;
            UIManager.SetGoldForUI(quantityGold);
        } 
    }
    public static DataWeapon[] HeroWeapons { get; private set; }
    public static DataWeapon CurrentWeapon { get => HeroWeapons[CurrentIndexWeapon]; }
    private static int quantityGold;
    private static IHealthHero healthHero;
    private static int countWeapon = 0;
    private HeroAttack heroAttack;

    void Start()
    {
        HeroWeapons = new DataWeapon[3];
        CurrentIndexWeapon = 0;
        countWeapon = 0;
        QuantityCoin = 0;
        HeroWeapons[0] = DataWeapon.GetDataWeapon(SettingPrefabs.Bonus.Weapon.StandartHeroWeapon, 0);
        
        healthHero = GetComponent<IHealthHero>();
        heroAttack = GetComponent<HeroAttack>();
        countWeapon++;
        UISelectWeapon.IsWeaponChoice = true;
        UIManager.SetWeaponForUI(CurrentIndexWeapon);
    }

    void FixedUpdate()
    {
        if (InputControllerGame.ButtonNextWeapon)
        {
            NextWeapon();
        }
        if (InputControllerGame.ButtonReload)
        {
            ReloadWeapon();
        }
    }

    private static void SelectWeapon(int indexWeapon)
    {
        CurrentIndexWeapon = indexWeapon;
        UISelectWeapon.IsWeaponChoice = true;
        UIManager.SetAmmoForUI();
    }

    private void NextWeapon()
    {
        var nextIndex = CurrentIndexWeapon + 1 == countWeapon ? 0 : CurrentWeapon.index + 1;
        if (nextIndex == CurrentIndexWeapon)
        {
            return;
        }
        heroAttack.StopReload();
        SelectWeapon(nextIndex);
    }

    private void ReloadWeapon()
    {
        if (CurrentWeapon.countBullet == CurrentWeapon.quantityBullets)
        {
            return;
        }
        heroAttack.StartReload();
    }

    public static void PickUpBonus(Bonus bonus) // TODO PickUpBonus(Bonus bonus, Inventory inventory)
    {
        switch (bonus)
        {
            case BonusCoin coin:
                AddInInventory(coin);
                break;
            case BonusWeapon weapon:
                AddInInventory(weapon);
                break;
            case BonusHeart heart:
                AddInInventory(heart);
                break;
            default:
                break;
        }
    }

    private static void AddInInventory(BonusHeart heart)
    {
        healthHero.ToHealthHero(heart.Health);
    }

    private static void AddInInventory(BonusWeapon weapon)
    {

        if (countWeapon<3)
        {
            var index = countWeapon;
            HeroWeapons[index] = DataWeapon.GetDataWeapon(weapon.dataWeapon, index);
            UIManager.SetWeaponForUI(index);
            SelectWeapon(index);

            countWeapon++;
            return;
        }
        HeroWeapons[CurrentIndexWeapon] = DataWeapon.GetDataWeapon(weapon.dataWeapon, CurrentIndexWeapon);
        UIManager.SetWeaponForUI(CurrentIndexWeapon);
        SelectWeapon(CurrentIndexWeapon);
    }

    private static void AddInInventory(BonusCoin coin)
    {
        QuantityCoin += coin.Coin;
    }
}
