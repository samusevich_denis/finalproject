﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum HeroState
{
    Ground,
    CrouchDown,
    StartJump,
    Air,
    Landing,
    StartFreeze,
    Freeze,
    StartPush,
    Push,
    SuperPush,
    Hurt,
    Wait,
}

public class HeroPlayerMovement : MonoBehaviour
{
    [SerializeField] private Transform pointRaycastAir;
    [SerializeField] private Transform pointRaycastPushTop;
    [SerializeField] private Transform pointRaycastPushMiddle;
    [SerializeField] private Transform pointRaycastPushBottom;
    public HeroState HeroState { get; private set; }
    public float TimerSkillPush
    {
        get => timerSkillPush; 
        set
        {
            timerSkillPush = value;
            UIManager.SetSkillForUI(timerSkillPush);
        }
    }

    private float paceSpeedPush = Settings.Hero.SkillPush.PaceSpeedPush;
    private float maxSpeed = Settings.Hero.MaxSpeed;
    private float speedMove = Settings.Hero.SpeedMove;
    private float forceJump = Settings.Hero.ForceJump;
    private int damageSuperPush = Settings.Hero.SkillPush.DamageSuperPush;
    private Vector2 sizeBoxCastPush = Settings.Hero.SkillPush.SizeBoxCastPush;
    private float forceSuperPushEnemy = Settings.Hero.SkillPush.ForceSuperPushEnemy;
    private float timeAnimationHurt = Settings.Hero.Animation.TimeAnimationHurt;
    private float adjustmentMoveAir = Settings.Hero.AdjustmentMoveAir;
    private bool isCanFreeze;
    private bool isSkillPushCharged;
    private float timerSkillPush;
    private float speedHero;
    private Vector2 direction;
    private Coroutine coroutineJump;
    private Coroutine coroutinePush;
    private Coroutine coroutineSuperPushCharged;
    private Rigidbody2D playerRigidbody2D;
    private Hero heroPlayer;
    private HeroAnimation heroAnimation;

    void Start()
    {
        playerRigidbody2D = GetComponent<Rigidbody2D>();
        heroPlayer = GetComponent<Hero>();
        heroAnimation = GetComponentInChildren<HeroAnimation>();
        HeroState = HeroState.Ground;
        speedHero = speedMove;
        isSkillPushCharged = true;
    }

    private void FixedUpdate()
    {
        if (heroPlayer.IsDie)
        {
            playerRigidbody2D.bodyType = RigidbodyType2D.Static;
            StopAllCoroutines();
            enabled = false;
            return;
        }
        direction = new Vector2(InputControllerGame.HorizontalAxis, 0f);

        switch (HeroState)
        {
            case HeroState.Ground:
                Move();
                StateTransitionMove();
                break;
            case HeroState.CrouchDown:
                CrouchDown();
                StateTransitionCrouchDown();
                break;
            case HeroState.StartJump:
                StartJump();
                break;
            case HeroState.Air:
                MoveAir(adjustmentMoveAir);
                StateTransitionAir();
                break;
            case HeroState.Landing:
                MoveAir(adjustmentMoveAir);
                StateTransitionLanding();
                break;
            case HeroState.StartFreeze:
                StartFreeze();
                break;
            case HeroState.Freeze:
                StateTransitionFreeze();
                break;
            case HeroState.StartPush:
                StartPush();
                break;
            case HeroState.Push:
                StateTransitionPush();
                break;
            case HeroState.SuperPush:
                StateTransitionPush();
                break;
            case HeroState.Hurt:
                Hurt();
                break;
            case HeroState.Wait:
                Move();
                break;
            default:
                break;
        }
        ControlVelocity();
    }

    private void Move()
    {
        float adjustment;
        if (heroPlayer.IsAttack)
        {
            adjustment = (heroPlayer.MousePosition.x > 0 && direction.x > 0 || heroPlayer.MousePosition.x < 0 && direction.x < 0) ? 1f : 0.5f;
        }
        else
        {
            adjustment = 1;
        }
        Vector2 velocity = playerRigidbody2D.velocity;
        velocity.x = direction.x * speedHero * adjustment;
        playerRigidbody2D.velocity = velocity;
    }
    private void StateTransitionMove()
    {
        if (InputControllerGame.ButtonDown)
        {
            HeroState = HeroState.CrouchDown;
            return;
        }
        if (InputControllerGame.ButtonJumpDown)
        {
            HeroState = HeroState.StartJump;
            isCanFreeze = true;
            return;
        }
        if (InputControllerGame.ButtonPushDown)
        {
            heroAnimation.SetTriggerAnimatorPush();
            HeroState = HeroState.StartPush;
            return;
        }
        if (heroPlayer.IsInAir)
        {
            isCanFreeze = true;
            HeroState = HeroState.Air;
        }
    }

    private void CrouchDown()
    {
        Vector2 velocity = playerRigidbody2D.velocity;
        velocity.x = Mathf.Lerp(0, velocity.x, 0.9f);
        playerRigidbody2D.velocity = velocity;
    }
    private void StateTransitionCrouchDown()
    {
        if (!InputControllerGame.ButtonDown)
        {
            HeroState = HeroState.Ground;
        }
        if (InputControllerGame.ButtonJumpDown)
        {
            HeroState = HeroState.StartJump;
            isCanFreeze = true;
            return;
        }
        if (InputControllerGame.ButtonPushDown)
        {
            heroAnimation.SetTriggerAnimatorPush();
            HeroState = HeroState.StartPush;
            return;
        }
    }

    private void StartJump()
    {
        if (coroutineJump != null)
        {
            return;
        }
        var velocity = playerRigidbody2D.velocity;
        velocity.y = 0;
        playerRigidbody2D.velocity = velocity;
        playerRigidbody2D.AddForce(Vector3.up * forceJump, ForceMode2D.Impulse);
        heroAnimation.SetTriggerAnimatorJump();
        StartCoroutine(DeferredState(HeroState.Air));
        HeroState = HeroState.Wait;
    }
    private void MoveAir(float adjustment)
    {
        Vector2 velocity = playerRigidbody2D.velocity;
        velocity.x = direction.x * speedHero * adjustment;
        playerRigidbody2D.velocity = velocity;
    }
    private void StateTransitionAir()
    {
        if (InputControllerGame.ButtonJumpDown && isCanFreeze)
        {
            HeroState = HeroState.StartFreeze;
            return;
        }
        if (InputControllerGame.ButtonPushDown)
        {
            heroAnimation.SetTriggerAnimatorPush();
            HeroState = HeroState.StartPush;
            return;
        }
        if (!heroPlayer.IsInAir)
        {
            HeroState = HeroState.Ground;
            return;
        }
        else
        {
            if (playerRigidbody2D.velocity.y < 0)
            {
                RaycastHit2D hitLanding = Physics2D.Raycast(pointRaycastAir.position, Vector2.down, 0.8f);
                if (hitLanding.collider != null && hitLanding.transform.TryGetComponent<TilemapCollider2D>(out TilemapCollider2D component))
                {
                    heroAnimation.SetTriggerAnimatorLanding();
                    HeroState = HeroState.Landing;
                }
            }
        }
    }
    private void StateTransitionLanding()
    {
        if (!heroPlayer.IsInAir)
        {
            StartCoroutine(Braking());
            HeroState = HeroState.Ground;
            return;
        }
        if (InputControllerGame.ButtonPushDown)
        {
            heroAnimation.SetTriggerAnimatorPush();
            HeroState = HeroState.StartPush;
            return;
        }
    }
    private IEnumerator Braking()
    {
        for (int i = 0; i < 6; i++)
        {
            speedHero *= 0.8f;
            yield return null;
        }
        speedHero = speedMove;
    }

    private void StartFreeze()
    {
        heroAnimation.SetTriggerAnimatorFreeze();
        playerRigidbody2D.constraints = RigidbodyConstraints2D.FreezeAll;
        StartCoroutine(DeferredState(HeroState.Freeze));
        HeroState = HeroState.Wait;
    }
    private void StateTransitionFreeze()
    {
        if (InputControllerGame.ButtonJumpDown)
        {
            StopFreeze();
            isCanFreeze = false;
            StartJump();
            return;
        }
        if (InputControllerGame.ButtonDown)
        {
            StopFreeze();
            isCanFreeze = false;
            StartFall();
            return;
        }
        if (InputControllerGame.ButtonPushDown)
        {
            StopFreeze();
            isCanFreeze = false;
            heroAnimation.SetTriggerAnimatorPush();
            HeroState = HeroState.StartPush;
            return;
        }
    }
    private void StopFreeze()
    {
        playerRigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
        HeroState = HeroState.Air;
    }
    private void StartFall()
    {
        playerRigidbody2D.AddForce(Vector3.down, ForceMode2D.Impulse);
    }

    private void StateTransitionPush()
    {
        if (coroutinePush !=null)
        {
            return;
        }
        HeroState = HeroState.Ground;
        playerRigidbody2D.bodyType = RigidbodyType2D.Dynamic;
    }

    private IEnumerator SkillCharged()
    {
        isSkillPushCharged = false;
        TimerSkillPush = Settings.Hero.SkillPush.TimeSkillReload;
        while(timerSkillPush > 0)
        {
            TimerSkillPush -= Time.deltaTime;
            yield return null;
        }
        isSkillPushCharged = true;
        coroutineSuperPushCharged = null;
    }
    private void StartPush()
    {
        playerRigidbody2D.velocity = Vector2.zero;
        playerRigidbody2D.bodyType = RigidbodyType2D.Kinematic;
        var wayPushTop = GetWay(pointRaycastPushTop, out List<Vector3> vectorPushTop, true);
        SetPositionRaycastPush();
        var wayPushMiddle = GetWay(pointRaycastPushMiddle, out List<Vector3> vectorPushMiddle, false);
        SetPositionRaycastPush();
        var wayPushBottom = GetWay(pointRaycastPushBottom, out List<Vector3> vectorPushBottom, false);
        SetPositionRaycastPush();
        var wayPush = wayPushTop;
        var directionPush = vectorPushTop;
        if (wayPush[0]> wayPushMiddle[0])
        {
            wayPush = wayPushMiddle;
            directionPush = vectorPushMiddle;
        }
        if (wayPush[0]> wayPushBottom[0])
        {
            wayPush = wayPushBottom;
            directionPush = vectorPushBottom;
        }
        if (isSkillPushCharged) 
        {
            HeroState = HeroState.SuperPush;
        }
        else
        {
            HeroState = HeroState.Push;
        }
        if (coroutineSuperPushCharged == null)
        {
            coroutineSuperPushCharged = StartCoroutine(SkillCharged());
        }
        coroutinePush =  StartCoroutine(MovePush(wayPush, directionPush, HeroState));
    }
    private List<float> GetWay(Transform pointRaycast, out List<Vector3> directionPush, bool isUpPointRaycast)
    {
        List<float> wayPush = new List<float>();
        directionPush = new List<Vector3>();
        float ray = 8f;
        Vector3 vectorRay = pointRaycast.right;
        while (ray > 0.1)
        {
            var hits = Physics2D.RaycastAll(pointRaycast.position, vectorRay, ray);
            if (TryGetCollider(hits, out var hit))
            {
                if (!hit.transform.TryGetComponent<InclinedPlatform>(out InclinedPlatform inclinedPlatform))
                {
                    wayPush.Add(pointRaycast.InverseTransformPoint(hit.point).x);
                    directionPush.Add(vectorRay);
                    return wayPush;
                }
                else
                {
                    wayPush.Add(pointRaycast.InverseTransformPoint(hit.point).x);
                    directionPush.Add(vectorRay);
                    var pos = (Vector3)hit.point;
                    if (isUpPointRaycast)
                    {
                        pos.y -= 0.1f;
                    }
                    else
                    {
                        pos.y += 0.1f;
                    }
                    pointRaycast.position = pos;
                    ray -= wayPush[wayPush.Count - 1];
                    vectorRay = inclinedPlatform.Inclined;
                    vectorRay.x *= pointRaycast.right.x;
                }
            }
            else
            {
                wayPush.Add(ray);
                directionPush.Add(vectorRay);
                return wayPush;
            }
        }
        return wayPush;
    }
    private bool TryGetCollider(RaycastHit2D[] raycastHits2D, out RaycastHit2D raycastHit2D)
    {
        foreach (var hit2D in raycastHits2D)
        {
            if (!hit2D.transform.TryGetComponent<TilemapCollider2D>(out TilemapCollider2D tilemapCollider2D))
            {
                continue;
            }
            if (hit2D.transform.TryGetComponent<PlatformEffector2D>(out PlatformEffector2D platformEffector2D))
            {
                continue;
            }
            raycastHit2D = hit2D;
            return true;
        }
        raycastHit2D = new RaycastHit2D();
        return false;
    }
    private void SetPositionRaycastPush()
    {
        pointRaycastPushTop.transform.localPosition = Constants.HeroEpsilon.PositionRaycastPushTop;
        pointRaycastPushMiddle.transform.localPosition = Constants.HeroEpsilon.PositionRaycastPushMiddle;
        pointRaycastPushBottom.transform.localPosition = Constants.HeroEpsilon.PositionRaycastPushBottom;
    }
    private IEnumerator MovePush(List<float> wayPush, List<Vector3> directionPush, HeroState heroState)
    {
        List<float> timers = new List<float>();
        var posEndPush = transform.position;
        for (int i = 0; i < wayPush.Count; i++)
        {
            timers.Add(wayPush[i] * paceSpeedPush);
        }
        for (int i = 0; i < wayPush.Count; i++)
        {
            var timerPush = timers[i];
            if (timers[i] >Time.fixedDeltaTime)
            {
                while (timers[i] > 0)
                {
                    var stepDirection = directionPush[i] * wayPush[i] * Time.fixedDeltaTime / timerPush;
                    if (heroState== HeroState.SuperPush)
                    {
                        SetDamageSuperPush(stepDirection);
                    }
                    transform.position += stepDirection;
                    yield return new WaitForSeconds(Time.fixedDeltaTime);
                    timers[i] -= Time.fixedDeltaTime;
                    if (timers[i] < Time.fixedDeltaTime)
                    {
                        timers[i] = -1f;
                    }
                }
            }
            posEndPush += directionPush[i] * wayPush[i];
            transform.position = posEndPush;
        }
        coroutinePush = null;
    }
    private void SetDamageSuperPush(Vector3 direction)
    {
        var offsetBoxCast = sizeBoxCastPush * 0.5f;
        var positionCast = (Vector2)transform.position + offsetBoxCast;
        var hits = Physics2D.BoxCastAll(positionCast, sizeBoxCastPush, 0f, direction, direction.magnitude);
        for (int j = 0; j < hits.Length; j++)
        {
            if (hits[j])
            {
                if (hits[j].transform != null && hits[j].transform.TryGetComponent<IHealthEnemy>(out IHealthEnemy enemy))
                {
                    var vectorEnemy = (Vector2)hits[j].transform.position - (Vector2)transform.position;
                    Vector2 vectorPushEnemy = Vector2.one;
                    vectorPushEnemy.x *= vectorEnemy.x > 0 ? 1 : -1;
                    enemy.ToDamageEnemy(vectorPushEnemy * forceSuperPushEnemy, damageSuperPush);
                }
            }

        }
    }

    public void StarHurt(Vector2 vector)
    {
        if (HeroState == HeroState.Freeze)
        {
            StopFreeze();
        }
        HeroState = HeroState.Hurt;
        playerRigidbody2D.AddForce(vector * 10, ForceMode2D.Impulse);
        StartCoroutine(DeferredState(HeroState.Air, timeAnimationHurt));
    }
    private void Hurt()
    {
        Vector2 velocity = playerRigidbody2D.velocity;
        velocity = Vector2.Lerp(Vector2.zero, velocity, 0.95f);
        playerRigidbody2D.velocity = velocity;
    }
    private void ControlVelocity()
    {
        Vector2 velocity = new Vector2(Mathf.Clamp(playerRigidbody2D.velocity.x, -maxSpeed, maxSpeed), Mathf.Clamp(playerRigidbody2D.velocity.y, -maxSpeed, maxSpeed));
        playerRigidbody2D.velocity = velocity;
    }

    private IEnumerator DeferredState(HeroState herostate)
    {
        yield return null;
        HeroState = herostate;
    }

    private IEnumerator DeferredState(HeroState herostate, float time)
    {
        yield return new WaitForSeconds(time);
        HeroState = herostate;
    }
}
