﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class HeroAnimation : MonoBehaviour
{
    [SerializeField] private Transform pointAngle;
    [SerializeField] private Animator animatorUp;
    [SerializeField] private Animator animatorDown;
    [SerializeField] private SpriteRenderer[] spritesHero;

    public bool IsLanding { get; private set; }
    public bool IsJump { get; private set; }

    private Hero heroPlayer;
    private HeroEffects heroEffects;
    private Coroutine coroutinePosition;
    private bool isPositionUp = true;

    void Start()
    {
        heroPlayer = GetComponentInParent<Hero>();
        heroEffects = GetComponentInChildren<HeroEffects>();
        animatorUp.transform.localPosition = Constants.HeroEpsilon.PositionHeroUp;
    }

    void Update()
    {
        if (heroPlayer.IsDie)
        {
            StopAllCoroutines();
            enabled = false;
        }
        SetPositionGraphics();
        SetDirectionGraphics();
        SetDataAnimator();
        SetDataEffect();
    }

    private void SetPositionGraphics()
    {
        if (coroutinePosition != null) { return; }
        if (isPositionUp && heroPlayer.IsCrouchDown)
        {
            coroutinePosition = StartCoroutine(SetPositionDown());
            isPositionUp = false;
            return;
        }
        if (!isPositionUp && !heroPlayer.IsCrouchDown)
        {
            coroutinePosition = StartCoroutine(SetPositionUp());
            isPositionUp = true;
            return;
        }
    }

    private IEnumerator SetPositionDown()
    {
        animatorUp.transform.localPosition = Constants.HeroEpsilon.PositionHeroUpCrouchMiddle;
        yield return new WaitForSeconds(Time.fixedDeltaTime);
        animatorUp.transform.localPosition = Constants.HeroEpsilon.PositionHeroUpCrouchDown;
        pointAngle.Translate(Constants.HeroEpsilon.VectorTranslatePointAngleDown);
        coroutinePosition = null;
    }

    private IEnumerator SetPositionUp()
    {
        animatorUp.transform.localPosition = Constants.HeroEpsilon.PositionHeroUp;
        pointAngle.Translate(Constants.HeroEpsilon.VectorTranslatePointAngleUp);
        yield return null;
        coroutinePosition = null;
    }

    private void SetDirectionGraphics()
    {
        float direction;
        if (heroPlayer.IsHurt)
        {
            if (heroPlayer.VelocityPlayer.x > 0) { direction = Constants.AngleForAxisY.DirectionalLeft; }
            else if (heroPlayer.VelocityPlayer.x<0) { direction = Constants.AngleForAxisY.DirectionRight; }
            else { return; }
        }
        else if (heroPlayer.IsAttack)
        {
            if (heroPlayer.MousePosition.x > 0) { direction = Constants.AngleForAxisY.DirectionRight; }
            else if (heroPlayer.MousePosition.x < 0) { direction = Constants.AngleForAxisY.DirectionalLeft; }
            else { return; }
        }
        else
        {
            if (heroPlayer.VelocityPlayer.x > 0.01) { direction = Constants.AngleForAxisY.DirectionRight; }
            else if (heroPlayer.VelocityPlayer.x < -0.01) { direction = Constants.AngleForAxisY.DirectionalLeft; }
            else if (InputControllerGame.HorizontalAxis > 0) { direction = Constants.AngleForAxisY.DirectionRight; }
            else if (InputControllerGame.HorizontalAxis < 0) { direction = Constants.AngleForAxisY.DirectionalLeft; }
            else { return; }
        }
        transform.localEulerAngles = new Vector3(0f, direction, 0f);
    }

    private void SetDataAnimator()
    {
        animatorUp.SetFloat("AngleAttack", heroPlayer.AngleAttack);
        animatorUp.SetBool("IsAttack", heroPlayer.IsAttack);
        animatorUp.SetBool("IsMove", heroPlayer.IsMove);
        animatorUp.SetBool("IsInAir", heroPlayer.IsInAir);

        animatorDown.SetBool("IsHurt", heroPlayer.IsHurt);
        animatorDown.SetBool("IsMove", heroPlayer.IsMove);
        animatorDown.SetBool("IsCrouchDown", heroPlayer.IsCrouchDown);
        animatorDown.SetBool("IsAttack", heroPlayer.IsAttack);
        animatorDown.SetBool("IsInAir", heroPlayer.IsInAir);
        animatorDown.SetBool("IsBackwardsMove", heroPlayer.IsBackwardsMove);
        animatorDown.SetBool("IsPush", heroPlayer.IsSuperPush);
        animatorDown.SetBool("IsPush", heroPlayer.IsPush);
        animatorDown.SetBool("IsSuperPush", heroPlayer.IsSuperPush);
    }

    private void SetDataEffect()
    {
        heroEffects.EffectFireFreezeActive(heroPlayer.IsFreeze);
        heroEffects.EffectSuperPushActive(heroPlayer.IsSuperPush);
        heroEffects.EffectPushActive(heroPlayer.IsPush);
    }
    public void SetTriggerAnimatorJump()
    {
        animatorDown.SetTrigger("Jump");
        StartCoroutine(SetIsJump(0.5f));
    }
    public void SetTriggerAnimatorLanding()
    {
        animatorDown.SetTrigger("Landing");
        StartCoroutine(SetIsLanding(0.3f));
    }
    public void SetTriggerAnimatorFall()
    {
        animatorDown.SetTrigger("Fall");
    }
    public void SetTriggerAnimatorFreeze()
    {
        animatorDown.SetTrigger("Freeze");
        heroEffects.FreezeExplosionsTrigger();
    }
    public void SetTriggerAnimatorPush()
    {
        animatorDown.SetTrigger("Push");
    }

    private IEnumerator SetIsLanding(float time)
    {
        IsLanding = true;
        yield return new WaitForSeconds(time);
        IsLanding = false;
    }

    private IEnumerator SetIsJump(float time)
    {
        IsJump = true;
        yield return new WaitForSeconds(time);
        IsJump = false;
    }

    public IEnumerator StartBlinking(float time)
    {
        float timer = Time.time + time;
        while (timer > Time.time)
        {
            for (int i = 0; i < spritesHero.Length; i++)
            {
                var color = spritesHero[i].color;
                if (color.a > 0.5f) { color.a = 0; }
                else { color.a = 1; }

                spritesHero[i].color = color;
            }
            yield return new WaitForSeconds(0.15f);
        }
        for (int i = 0; i < spritesHero.Length; i++)
        {
            var color = spritesHero[i].color;
            color.a = 1;
            spritesHero[i].color = color;
        }
    }
}
