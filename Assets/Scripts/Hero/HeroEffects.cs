﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroEffects : MonoBehaviour
{
    [SerializeField] private Animator[] effectFreezeExplosions;
    [SerializeField] private Animator effectFireFreeze;
    [SerializeField] private ParticleSystem effectSuperPush;
    [SerializeField] private ParticleSystem effectPush;
    [SerializeField] private ParticleSystem effectDie;

    private bool effectSuperPushTrigger;
    private bool effectPushTrigger;
    private bool effectDieTrigger;

    private void Start()
    {
        effectSuperPushTrigger = true;
        effectPushTrigger = true;
        effectDieTrigger = true;
        effectSuperPush.Stop();
        effectPush.Stop();
        effectDie.Stop();
    }

    public void FreezeExplosionsTrigger()
    {
        effectFreezeExplosions[0].SetTrigger("ExplosionUpLeft");
        effectFreezeExplosions[1].SetTrigger("ExplosionMiddleLeft");
        effectFreezeExplosions[2].SetTrigger("ExplosionDownLeft");
    }

    public void EffectFireFreezeActive(bool isActive)
    {
        effectFireFreeze.SetBool("IsFreeze", isActive);
    }

    public void EffectSuperPushActive(bool isActive)
    {
        EffectActive(isActive, effectSuperPush, ref effectSuperPushTrigger);
    }

    public void EffectPushActive(bool isActive)
    {
        EffectActive(isActive, effectPush, ref effectPushTrigger);
    }

    public void EffectDieActive(bool isActive)
    {
        EffectActive(isActive, effectDie, ref effectDieTrigger);
    }

    private void EffectActive(bool isActive, ParticleSystem effect, ref bool trigger)
    {
        if (trigger == isActive)
        {
            if (trigger)
            {
                effect.Play();
                trigger = false;
            }
            else
            {
                effect.Stop();
                trigger = true;
            }
        }
    }
}
