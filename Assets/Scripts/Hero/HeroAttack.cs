﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroAttack : MonoBehaviour
{

    [SerializeField] private Transform pointShot;
    [SerializeField] private Transform PointAngle;
    private Hero heroPlayer;
    private Coroutine coroutineShot;
    private Coroutine coroutineReload;
    private bool triggerAttack;
    private bool isReload;
    private bool isShotReload;

    void Start()
    {
        heroPlayer = GetComponent<Hero>();
        triggerAttack = true;
    }

    void Update()
    {
        if (heroPlayer.IsDie)
        {
            StopAllCoroutines();
            enabled = false;
        }
        if (heroPlayer.IsAttack == triggerAttack)
        {
            if (triggerAttack)
            {
                if (isShotReload || isReload)
                {
                    return;
                }
                triggerAttack = false;
                coroutineShot = StartCoroutine(StartShotWeapon());
            }
            else
            {
                triggerAttack = true;
                StopCoroutine(coroutineShot);
            }
        }
    }

    private IEnumerator StartShotWeapon()
    {
        while (heroPlayer.IsAttack)
        {
            while (isReload || isShotReload)
            {
                yield return null;
            }
            if (Inventory.CurrentWeapon.countBullet == 0)
            {
                coroutineReload = StartCoroutine(TimeReload(Inventory.CurrentWeapon.reloadTime));
                continue;
            }
            ShotActiveWeapon();
            StartCoroutine(TimeShotReload(Inventory.CurrentWeapon.shotTime));
            yield return null;
        }
    }

    private void ShotActiveWeapon()
    {
        Inventory.CurrentWeapon.countBullet--;
        UIManager.SetAmmoForUI();
        var obj = Instantiate(Inventory.CurrentWeapon.bullet, pointShot.position, Quaternion.identity);
        var bullet = obj.GetComponent<Bullet>();
        Vector3 directionBullet;
        if ((InputControllerGame.MousePositionToWorld - PointAngle.position).sqrMagnitude < 0.5f)
        {
            directionBullet = pointShot.right;
        }
        else
        {
            directionBullet = InputControllerGame.MousePositionToWorld - pointShot.position;
        }
        bullet.SetDataBullet(Inventory.CurrentWeapon, directionBullet.normalized);
        bullet.Launch();
    }

    private IEnumerator TimeShotReload(float time)
    {
        isShotReload = true;
        yield return new WaitForSeconds(time);
        isShotReload = false;
    }

    public void StartReload()
    {
        if (coroutineReload == null)
        {
            coroutineReload = StartCoroutine(TimeReload(Inventory.CurrentWeapon.reloadTime));
        }
    }

    public void StopReload()
    {
        if (coroutineReload!=null)
        {
            isReload = false;
            StopCoroutine(coroutineReload);
        }
    }

    private IEnumerator TimeReload(float time)
    {
        isReload = true;
        while (time>0)
        {
            UIManager.SetRelloadForUI(time);
            yield return null;
            time -= Time.deltaTime;
        }
        Inventory.CurrentWeapon.countBullet = Inventory.CurrentWeapon.quantityBullets;
        isReload = false;
        coroutineReload = null;
    }
}
