﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Hero : MonoBehaviour, IHealthHero
{
    [SerializeField] private Transform pointAngle;
    [SerializeField] private Transform[] pointsRaycastAir;

    public Vector3 MousePosition { get; private set; }
    public Vector2 VelocityPlayer { get; private set; }
    public bool IsAttack { get; private set; }
    public bool IsInAir { get; private set; }
    public bool IsMove { get; private set; }
    public bool IsCrouchDown
    {
        get => isCrouchDown;
        private set
        {
            if (value != isCrouchDown)
            {
                if (value)
                {
                    var size = sizeColider;
                    size.y *= Settings.Hero.AdjustmentColliderCrouchDown;
                    colliderHero.size = size;
                    colliderHero.offset *= Settings.Hero.AdjustmentColliderCrouchDown;
                }
                else
                {
                    colliderHero.size = sizeColider;
                    colliderHero.offset = new Vector2(0, sizeColider.y * 0.5f);
                }
            }
            isCrouchDown = value;
        }
    }
    public bool IsBackwardsMove { get; private set; }
    public bool IsLanding { get; private set; }
    public bool IsJump { get; private set; }
    public bool IsFall { get; private set; }
    public bool IsPush { get; private set; }
    public bool IsFreeze { get; private set; }
    public bool IsSuperPush { get; private set; }
    public bool IsHurt { get; private set; }
    public bool IsDie { get; private set; }
    public bool IsInvulnerabilityHero { get; set; }
    public float AngleAttack { get; private set; }
    public int HealthHero
    {
        get => healthHero;
        set
        {
            if (IsDie) { return; }

            if (value > healthHero) { }
            else if (IsInvulnerabilityHero) { return; }

            healthHero = Mathf.Clamp(value, 0, Settings.Hero.MaxHealth);
            UIManager.SetHealthForUI(healthHero);

            if (healthHero == 0) { DieHero(); }
        }
    }
    private int healthHero = Settings.Hero.MaxHealth;
    private float timeInvulnerabilityAfterDamage = Settings.Hero.TimeInvulnerabilityAfterDamage;

    private bool isCrouchDown;
    private Vector2 sizeColider;
    private CapsuleCollider2D colliderHero;
    private Rigidbody2D playerRigidbody2D;
    private Coroutine coroutineFall;
    private Coroutine coroutineInvulnerability;
    private HeroPlayerMovement playerMovement;
    private HeroAnimation heroAnimation;
    private HeroEffects heroEffects;

    private void Start()
    {
        playerRigidbody2D = transform.gameObject.GetComponent<Rigidbody2D>();
        playerMovement = transform.gameObject.GetComponent<HeroPlayerMovement>();
        heroEffects = GetComponentInChildren<HeroEffects>();
        heroAnimation = GetComponentInChildren<HeroAnimation>();
        colliderHero = GetComponent<CapsuleCollider2D>();
        sizeColider = colliderHero.size;
    }

    private void FixedUpdate()
    {
        VelocityPlayer = playerRigidbody2D.velocity;
        MousePosition = pointAngle.InverseTransformPoint(InputControllerGame.MousePositionToWorld);
        IsJump = heroAnimation.IsJump;
        IsLanding = heroAnimation.IsLanding;
        IsCrouchDown = InputControllerGame.ButtonDown;
        IsHurt = GetIsHurt();
        IsFreeze = GetIsFreeze();
        IsInAir = GetIsInAir();
        IsPush = GetIsPush();
        IsAttack = GetIsAttack();
        IsMove = GetIsMove();
        IsFall = GetIsFail();
        IsSuperPush = GetIsSuperPush();

        IsBackwardsMove = GetBackwardsMove();
        AngleAttack = Vector3.Angle(MousePosition, Vector3.down);
    }

    private bool GetIsHurt()
    {
        return playerMovement.HeroState == HeroState.Hurt;
    }

    private bool GetIsSuperPush()
    {
        return playerMovement.HeroState == HeroState.SuperPush;
    }

    private bool GetIsAttack()
    {
        if (IsInAir && !IsFreeze || IsJump || IsLanding || IsHurt || IsPush)
        {
            return false;
        }
        return InputControllerGame.MouseButtonDown;
    }

    private bool GetIsFreeze()
    {
        return playerMovement.HeroState == HeroState.Freeze;
    }

    private bool GetIsPush()
    {
        return playerMovement.HeroState == HeroState.Push || playerMovement.HeroState == HeroState.SuperPush;
    }

    private bool GetIsInAir()
    {
        if (IsJump) { return true; }
        foreach (var point in pointsRaycastAir)
        {
            RaycastHit2D hit = Physics2D.Raycast(point.position, Vector2.down, 0.2f);
            if (hit.collider != null)
            {
                var component = hit.transform.GetComponent<TilemapCollider2D>();
                if (component != null) { return false; }
            }
        }
        return true;
    }

    private bool GetIsMove()
    {
        if (IsCrouchDown) { return false; }
        if (!InputControllerGame.ButtonMove) { return false; }
        if (IsInAir) { return false; }

        return true;
    }
    private bool GetBackwardsMove()
    {
        if (MousePosition.x > 0 && VelocityPlayer.x > 0 || MousePosition.x < 0 && VelocityPlayer.x < 0) { return false; }

        return true;
    }
    private bool GetIsFail()
    {
        if (IsInAir && VelocityPlayer.y < 0)
        {
            if (coroutineFall == null)
            {
                coroutineFall = StartCoroutine(SetTriggerFall());
            }
            return true;
        }
        else { return false; }
    }
    private IEnumerator SetTriggerFall()
    {
        heroAnimation.SetTriggerAnimatorFall();
        while (IsInAir && !IsPush)
        {
            yield return null;
        }
        yield return null;
        coroutineFall = null;
    }

    public void ToDamageHero(Vector2 vectorPush, int damage)
    {
        if (coroutineInvulnerability != null || IsPush) { return; }

        HealthHero -= damage;
        coroutineInvulnerability = StartCoroutine(ChangeInvulnerabilityHero(timeInvulnerabilityAfterDamage));
        playerMovement.StarHurt(vectorPush);
    }
    private IEnumerator ChangeInvulnerabilityHero(float time)
    {
        IsInvulnerabilityHero = true;
        yield return StartCoroutine(heroAnimation.StartBlinking(time));
        IsInvulnerabilityHero = false;
        coroutineInvulnerability = null;
    }
    public void DieHero()
    {
        IsDie = true;
        heroEffects.EffectDieActive(true);
        ((GameController)Controller.GetController()).GameOver();
    }

    public void ToHealthHero(int heal)
    {
        HealthHero += heal;
    }
}
