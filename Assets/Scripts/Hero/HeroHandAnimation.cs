﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroHandAnimation : MonoBehaviour
{
    private Hero heroPlayer;
    private Transform transformJoint;
    private Animator animatorHand;
    private Transform transformPointShot;

    void Start()
    {
        animatorHand = transform.GetComponent<Animator>();
        heroPlayer = GetComponentInParent<Hero>();
        transformJoint = transform.parent;
        transformPointShot = transform.GetChild(0);
    }

    void Update()
    {
        SetDataAnimatorHand();
        transform.localRotation = Quaternion.Euler(0f, 0f, GetAngleHand() - GetAnglePosition());
        transformPointShot.rotation = Quaternion.FromToRotation(Vector3.right,heroPlayer.MousePosition);
    }

    private void SetDataAnimatorHand()
    {
        animatorHand.SetFloat("AngleAttack", heroPlayer.AngleAttack);
        animatorHand.SetBool("IsAttack", heroPlayer.IsAttack);
        animatorHand.SetBool("IsMove", heroPlayer.IsMove);
    }

    private float GetAnglePosition()
    {
        float anglePosition = 0;
        if (heroPlayer.IsMove)
        {
            if (heroPlayer.AngleAttack > 0f && heroPlayer.AngleAttack < 22.5f)
            {
                transformJoint.localPosition = Constants.HeroEpsilon.PositionHandMove_000;
                transformPointShot.localPosition = Constants.HeroEpsilon.PositionPointShotMove_000;

                anglePosition = 0f;
            }
            else if (heroPlayer.AngleAttack > 22.5f && heroPlayer.AngleAttack < 67.5f)
            {
                transformJoint.localPosition = Constants.HeroEpsilon.PositionHandMove_045;
                transformPointShot.localPosition = Constants.HeroEpsilon.PositionPointShotMove_045;
   
                anglePosition = 45f;
            }
            else if (heroPlayer.AngleAttack > 67.5f && heroPlayer.AngleAttack < 112.5f)
            {
                transformJoint.localPosition = Constants.HeroEpsilon.PositionHandMove_090;
                transformPointShot.localPosition = Constants.HeroEpsilon.PositionPointShotMove_090;

                anglePosition = 90f;
            }
            else if (heroPlayer.AngleAttack > 112.5f && heroPlayer.AngleAttack < 157.5f)
            {
                transformJoint.localPosition = Constants.HeroEpsilon.PositionHandMove_135;
                transformPointShot.localPosition = Constants.HeroEpsilon.PositionPointShotMove_135;

                anglePosition = 135f;
            }
            else if (heroPlayer.AngleAttack > 157.5f && heroPlayer.AngleAttack < 180f)
            {
                transformJoint.localPosition = Constants.HeroEpsilon.PositionHandMove_180;
                transformPointShot.localPosition = Constants.HeroEpsilon.PositionPointShotMove_180;

                anglePosition = 180f;
            }
        }
        else
        {
            if (heroPlayer.AngleAttack > 0f && heroPlayer.AngleAttack < 22.5f)
            {
                transformJoint.localPosition = Constants.HeroEpsilon.PositionHandIdle_000;
                transformPointShot.localPosition = Constants.HeroEpsilon.PositionPointShotIdle_000;

                anglePosition = 0f;
            }
            else if (heroPlayer.AngleAttack > 22.5f && heroPlayer.AngleAttack < 67.5f)
            {
                transformJoint.localPosition = Constants.HeroEpsilon.PositionHandIdle_045;
                transformPointShot.localPosition = Constants.HeroEpsilon.PositionPointShotIdle_045;

                anglePosition = 45f;
            }
            else if (heroPlayer.AngleAttack > 67.5f && heroPlayer.AngleAttack < 112.5f)
            {
                transformJoint.localPosition = Constants.HeroEpsilon.PositionHandIdle_090;
                transformPointShot.localPosition = Constants.HeroEpsilon.PositionPointShotIdle_090;

                anglePosition = 90f;
            }
            else if (heroPlayer.AngleAttack > 112.5f && heroPlayer.AngleAttack < 157.5f)
            {
                transformJoint.localPosition = Constants.HeroEpsilon.PositionHandIdle_135;
                transformPointShot.localPosition = Constants.HeroEpsilon.PositionPointShotIdle_135;
    
                anglePosition = 135f;
            }
            else if (heroPlayer.AngleAttack > 157.5f && heroPlayer.AngleAttack < 180f)
            {
                transformJoint.localPosition = Constants.HeroEpsilon.PositionHandIdle_180;
                transformPointShot.localPosition = Constants.HeroEpsilon.PositionPointShotIdle_180;

                anglePosition = 180f;
            }
        }
        return anglePosition;
    }

    private float GetAngleHand()
    {
        float angleHand;
        var pos = transformJoint.InverseTransformPoint(InputControllerGame.MousePositionToWorld);
        if (pos.sqrMagnitude<0.3f)
        {
            angleHand = heroPlayer.AngleAttack;
        }
        else
        {
            angleHand = Vector3.Angle(transformJoint.InverseTransformPoint(InputControllerGame.MousePositionToWorld), Vector3.down);
            if (pos.x < 0)
            {
                angleHand *= -1;
            }
        }
        return angleHand;
    }
}
