﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHealthHero
{
    int HealthHero { get; set; }
    bool IsInvulnerabilityHero { get; set; }
    void ToDamageHero(Vector2 vectorPush, int damage);
    void ToHealthHero(int heal);
    void DieHero();

}
