﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BulletRifle : Bullet
{
    protected LineRenderer lineRenderer;

    public override void SetDataBullet(DataWeapon dataWeapon, Vector3 direction)
    {
        this.direction = direction;
        damage = dataWeapon.damage;
        speedBullet = dataWeapon.speedBullet;
        timerLifeBullet = dataWeapon.timerLifeBullet;
        forcePushEnemy = dataWeapon.forcePushEnemy;
        lineRenderer = GetComponent<LineRenderer>();
        transform.rotation = Quaternion.FromToRotation(transform.right, direction);
        isBulletMove = true;
        timerDestroyBullet = 1f;
    }

    protected override IEnumerator BulletLaunch()
    {
        lineRenderer.SetPosition(0, transform.position);
        while (isBulletMove)
        {
            var hit = Physics2D.Raycast(transform.position, direction, speedBullet * Time.deltaTime);
            transform.position += direction * speedBullet * Time.deltaTime;
            if (hit.transform != null)
            {
                if (hit.transform.TryGetComponent<TilemapCollider2D>(out TilemapCollider2D tilemapCollider2D) && tilemapCollider2D.gameObject.layer != Constants.Layer.SmallPlatform)
                {
                    transform.position = hit.point;
                    HitBullet();
                }
                if (hit.transform.TryGetComponent<IHealthEnemy>(out IHealthEnemy enemy))
                {
                    EffectEnemy.SetEffect(hit.transform.gameObject, effect);
                    enemy.ToDamageEnemy(direction * forcePushEnemy, damage);
                    transform.position = hit.point;
                    HitBullet();
                }
            }
            lineRenderer.SetPosition(1, transform.position);
            yield return null;
        }
        yield return null;
    }

    protected override void HitBullet()
    {
        isBulletMove = false;
        StartCoroutine(DestroyBullet(timerDestroyBullet));
        enabled = false;
    }

    protected override IEnumerator DestroyBullet(float timerDestroyBullet)
    {
        var time = timerDestroyBullet;
        while (timerDestroyBullet>0)
        {
            var color = lineRenderer.startColor;
            color.a = timerDestroyBullet / time;
            lineRenderer.startColor = lineRenderer.endColor = color;
            timerDestroyBullet -= Time.deltaTime;
            yield return null;
        }
        yield return null;
        Destroy(gameObject);
    }
}
