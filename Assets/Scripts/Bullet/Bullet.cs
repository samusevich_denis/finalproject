﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Bullet : MonoBehaviour
{
    public TypeEffectForBullet effect;
    protected float speedBullet;
    protected int damage;
    protected float timerLifeBullet;
    protected float forcePushEnemy;
    protected Vector3 direction;
    private Animator AnimatorHit;
    protected float timerDestroyBullet = 2;
    protected bool isBulletMove;
    protected Coroutine coroutineLunch;

    public virtual void SetDataBullet(DataWeapon dataWeapon, Vector3 direction)
    {
        this.direction = direction;
        damage = dataWeapon.damage;
        speedBullet = dataWeapon.speedBullet;
        timerLifeBullet = dataWeapon.timerLifeBullet;
        forcePushEnemy = dataWeapon.forcePushEnemy;
        AnimatorHit = transform.GetComponent<Animator>();
        transform.rotation = Quaternion.FromToRotation(transform.right, direction);
        isBulletMove = true;
    }

    protected virtual void Update()
    {
        if (!isBulletMove) { return; }
        if (timerLifeBullet < 0) { HitBullet(); }

        timerLifeBullet -= Time.deltaTime;
    }
    public void Launch()
    {
        coroutineLunch = StartCoroutine(BulletLaunch());
    }

    protected virtual IEnumerator BulletLaunch()
    {
        AnimatorHit.SetTrigger("Init");
        while (isBulletMove)
        {
            var pos = transform.position;
            pos += direction * speedBullet * Time.deltaTime;
            transform.position = pos;
            yield return null;
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isBulletMove) { return; }
        if (collision.transform.TryGetComponent<TilemapCollider2D>(out TilemapCollider2D tilemapCollider2D) &&
            tilemapCollider2D.gameObject.layer != Constants.Layer.SmallPlatform)
        {
            HitBullet();
        }
        if (collision.transform.TryGetComponent<IHealthEnemy>(out IHealthEnemy enemy))
        {
            EffectEnemy.SetEffect(collision.transform.gameObject, effect);
            enemy.ToDamageEnemy(direction * forcePushEnemy, damage);
            HitBullet();
        }
    }

    protected virtual void HitBullet()
    {
        isBulletMove = false;
        AnimatorHit.SetTrigger("Hit");
        gameObject.layer = Constants.Layer.Effects;
        StartCoroutine(DestroyBullet(timerDestroyBullet));
        enabled = false;
    }

    protected virtual IEnumerator DestroyBullet(float timerDestroyBullet)
    {
        yield return new WaitForSeconds(timerDestroyBullet);
        Destroy(gameObject);
    }
}