﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BulletSniper : BulletRifle
{
    protected override IEnumerator BulletLaunch()
    {
        lineRenderer.SetPosition(0, transform.position);
        while (isBulletMove)
        {
            var hits = Physics2D.RaycastAll(transform.position, direction, speedBullet * Time.deltaTime);
            transform.position += direction * speedBullet * Time.deltaTime;
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].transform != null && hits[i].transform.TryGetComponent<TilemapCollider2D>(out TilemapCollider2D tilemapCollider2D) && tilemapCollider2D.gameObject.layer != Constants.Layer.SmallPlatform)
                {
                    transform.position = hits[i].point;
                    lineRenderer.SetPosition(1, transform.position);
                    HitBullet();
                    break;
                }
                if (hits[i].transform != null && hits[i].transform.TryGetComponent<IHealthEnemy>(out IHealthEnemy enemy))
                {
                    EffectEnemy.SetEffect(hits[i].transform.gameObject, effect);
                    enemy.ToDamageEnemy(direction * forcePushEnemy, damage);
                }
            }
            lineRenderer.SetPosition(1, transform.position);
            yield return null;
        }
        yield return null;
    }
}
