﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public int FireDamage { get; set; } = Settings.Effects.Damage.Fire;

    private void Start()
    {
        Destroy(this.gameObject, 3f);
    }
    
    private void Update()
    {
        transform.Translate(new Vector3(Random.Range(-1, 1)==0?1:-1, 0f, 0f)*Time.deltaTime);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform != null && collision.transform.TryGetComponent<IHealthEnemy>(out IHealthEnemy healthEnemy))
        {
            healthEnemy.ToDamageEnemy(Vector3.zero, FireDamage);
        }
    }
}
