﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectLightning : EffectEnemy
{
    private GameObject prefabLightning;
    private EnemyMovement enemyMovement;
    private GameObject lightningObject;
    private Rigidbody2D enemyrigidbody2D;

    void Start()
    {
        TimerEffect = Settings.Effects.Timer.Lightning;
        prefabLightning = SettingPrefabs.EffectEnemy.Lightning;
        enemyMovement = gameObject.GetComponent<EnemyMovement>();
        enemyMovement.enabled = false;
        enemyMovement.StateEnemy = StateEnemyStandart.Wait;
        enemyrigidbody2D = gameObject.GetComponent<Rigidbody2D>();
        lightningObject = Instantiate(prefabLightning, enemyMovement.transform.position, Quaternion.identity, enemyMovement.transform);
    }

    void Update()
    {
        if (TimerEffect < 0) { Destroy(this); }

        TimerEffect -= Time.deltaTime;
        enemyrigidbody2D.velocity = new Vector2(0f, enemyrigidbody2D.velocity.y);
    }

    protected override void AddEffect() { }
    protected void OnDestroy()
    {
        if (enemyMovement == null) { return; }
 
        enemyMovement.enabled = true;
        Destroy(lightningObject);
    }
}
