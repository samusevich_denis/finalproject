﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectAcid : EffectEnemy
{
    private int acidDamage = Settings.Effects.Damage.Acid;
    private float speedAcidDamage = Settings.Effects.Damage.SpeedAcid;
    private Color colorAcid = Settings.Effects.ColorEffect.Acid;
    private float timerCloud = Settings.Effects.Additionally.AcidTimerCloudInstantiate;
    private float speedCloud = Settings.Effects.Additionally.AcidSpeedMoveCloud;
    private GameObject prefabAcidCloud;

    private SpriteRenderer spriteRenderer;
    private Color oldColor;
    private IHealthEnemy healthEnemy;
    private Transform[] acidCloud = new Transform[2];
    Coroutine[] coroutinesMoveCloud = new Coroutine[2];
    private Coroutine coroutineAcidDamage;
    private float timer;
    private void Start()
    {
        TimerEffect = Settings.Effects.Timer.Acid;
        prefabAcidCloud = SettingPrefabs.EffectEnemy.AcidCloud;
        spriteRenderer = gameObject.GetComponentInChildren<SpriteRenderer>();
        oldColor = spriteRenderer.color;
        spriteRenderer.color = colorAcid;
        healthEnemy = gameObject.GetComponentInChildren<IHealthEnemy>();
        coroutineAcidDamage = StartCoroutine(SetDamageAcid());
        acidCloud[0] = Instantiate(prefabAcidCloud, transform.position, Quaternion.identity).transform;
        acidCloud[0].gameObject.SetActive(false);
        acidCloud[1] = Instantiate(prefabAcidCloud, transform.position, Quaternion.identity).transform;
        acidCloud[1].gameObject.SetActive(false);
    }

    private void Update()
    {
        if (TimerEffect < 0)
        {
            Destroy(this);
        }
        TimerEffect -= Time.deltaTime;
        MoveAcidCloud();
    }

    private void MoveAcidCloud()
    {
        if (acidCloud[0].gameObject.activeSelf == false)
        {
            acidCloud[0].gameObject.SetActive(true);
            coroutinesMoveCloud[0] = StartCoroutine(MoveAcidCloud(acidCloud[0]));
            timer = timerCloud*0.5f;
        }
        if (acidCloud[1].gameObject.activeSelf == false && timer > 0f)
        {
            timer -= Time.deltaTime;
            return;
        }
        else if (acidCloud[1].gameObject.activeSelf == false)
        {
            acidCloud[1].gameObject.SetActive(true);
            coroutinesMoveCloud[1] = StartCoroutine(MoveAcidCloud(acidCloud[1]));
        }
    }

    private IEnumerator MoveAcidCloud(Transform transformCloud)
    {
        while (true)
        {
            transformCloud.position = transform.position;
            float timer = timerCloud;
            while (timer>0)
            {
                transformCloud.Translate(Vector3.up * speedCloud * Time.deltaTime);
                timer -= Time.deltaTime;
                yield return null;
            }
            yield return null;
        }
    }

    private IEnumerator SetDamageAcid()
    {
        while (true)
        {
            yield return new WaitForSeconds(speedAcidDamage);
            healthEnemy.ToDamageEnemy(Vector2.zero, acidDamage);
        }
    }

    protected override void AddEffect()
    {
        acidDamage += Settings.Effects.Damage.Acid;
    }

    protected void OnDestroy()
    {
        if (spriteRenderer == null)
        {
            return;
        }
        for (int i = 0; i < coroutinesMoveCloud.Length; i++)
        {
            if (coroutinesMoveCloud[i]!=null) {StopCoroutine(coroutinesMoveCloud[i]);}
        }
        StopCoroutine(coroutineAcidDamage);
        spriteRenderer.color = oldColor;
        Destroy(acidCloud[0]?.gameObject);
        Destroy(acidCloud[1]?.gameObject);
    }
}
