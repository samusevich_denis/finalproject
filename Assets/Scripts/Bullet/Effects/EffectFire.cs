﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectFire : EffectEnemy
{
    private float speedFireDamage = Settings.Effects.Damage.SpeedFire;
    private int fireDamage = Settings.Effects.Damage.Fire;
    private Color colorFire = Settings.Effects.ColorEffect.Fire;
    private float timerFireInstantiate = Settings.Effects.Additionally.FireTimerInstantiate;

    private GameObject prefabFire;
    private SpriteRenderer spriteRenderer;
    private Color oldColor;
    private IHealthEnemy healthEnemy;
    private Coroutine coroutineFireDamage;
    private Coroutine coroutineFireInstantiate;

    void Start()
    {
        TimerEffect = Settings.Effects.Timer.Fire;
        prefabFire = SettingPrefabs.EffectEnemy.Fire;
        spriteRenderer = gameObject.GetComponentInChildren<SpriteRenderer>();
        oldColor = spriteRenderer.color;
        spriteRenderer.color = colorFire;
        healthEnemy = gameObject.GetComponentInChildren<IHealthEnemy>();
        coroutineFireDamage = StartCoroutine(SetDamageFire());
        coroutineFireInstantiate = StartCoroutine(FireInstantiate());
    }

    void Update()
    {
        if (TimerEffect < 0) { Destroy(this); }

        TimerEffect -= Time.deltaTime;
    }

    private IEnumerator SetDamageFire()
    {
        while (true)
        {
            yield return new WaitForSeconds(speedFireDamage);
            healthEnemy.ToDamageEnemy(Vector2.zero, fireDamage);
        }
    }

    private IEnumerator FireInstantiate()
    {
        while (true)
        {
            yield return new WaitForSeconds(timerFireInstantiate);
            var obj = Instantiate(prefabFire, transform.position, Quaternion.identity);
            obj.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1, 2), 1f).normalized, ForceMode2D.Impulse);
        }
    }

    protected override void AddEffect() { }
    protected void OnDestroy()
    {
        if (spriteRenderer == null) { return; }

        StopCoroutine(coroutineFireDamage);
        StopCoroutine(coroutineFireInstantiate);
        spriteRenderer.color = oldColor;
    }
}
