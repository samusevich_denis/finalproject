﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectFreeze : EffectEnemy
{
    private Color colorFreeze = Settings.Effects.ColorEffect.Freeze;
    private float freezeBrakingPower = Settings.Effects.Additionally.FreezeBrakingPower;

    private SpriteRenderer spriteRenderer;
    private Color oldColor;
    private EnemyMovement enemyMovement;
    private float oldMoveSpeed;
    private float oldRunSpeed;
    private float oldJumpForse;
    private Animator animator;
    private float oldSpeedAnimator;

    void Start()
    {
        TimerEffect = Settings.Effects.Timer.Freeze;
        spriteRenderer = gameObject.GetComponentInChildren<SpriteRenderer>();
        oldColor = spriteRenderer.color;
        spriteRenderer.color = colorFreeze;
        enemyMovement = gameObject.GetComponent<EnemyMovement>();
        oldMoveSpeed = enemyMovement.MoveSpeed;
        enemyMovement.MoveSpeed *= freezeBrakingPower;
        oldRunSpeed = enemyMovement.RunSpeed;
        enemyMovement.RunSpeed *= freezeBrakingPower;
        oldJumpForse = enemyMovement.JumpForce;
        enemyMovement.JumpForce *= freezeBrakingPower;
        animator = gameObject.GetComponentInChildren<Animator>();
        oldSpeedAnimator = animator.speed;
        animator.speed *= freezeBrakingPower;
    }

    void Update()
    {
        if (TimerEffect < 0) { Destroy(this); }

        TimerEffect -= Time.deltaTime;
    }

    protected override void AddEffect() { }
    protected void OnDestroy()
    {
        if (spriteRenderer == null) { return; }

        spriteRenderer.color = oldColor;
        enemyMovement.MoveSpeed = oldMoveSpeed;
        enemyMovement.RunSpeed = oldRunSpeed;
        enemyMovement.JumpForce = oldJumpForse;
        animator.speed = oldSpeedAnimator;
    }
}
