﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum TypeEffectForBullet
{
    None,
    Freeze,
    Acid,
    Fire,
    Lightning,
}

public abstract class EffectEnemy : MonoBehaviour
{
    protected float TimerEffect { get; set; }

    public static void SetEffect(GameObject enemy, TypeEffectForBullet effect)
    {
        switch (effect)
        {
            case TypeEffectForBullet.None:
                break;
            case TypeEffectForBullet.Freeze:
                SetEffectEnemy<EffectFreeze>(enemy, Settings.Effects.Timer.Freeze);
                break;
            case TypeEffectForBullet.Acid:
                SetEffectEnemy<EffectAcid>(enemy, Settings.Effects.Timer.Acid);
                break;
            case TypeEffectForBullet.Fire:
                SetEffectEnemy<EffectFire>(enemy, Settings.Effects.Timer.Fire);
                break;
            case TypeEffectForBullet.Lightning:
                SetEffectEnemy<EffectLightning>(enemy, Settings.Effects.Timer.Lightning);
                break;
            default:
                break;
        }
    }

    public static void SetEffectEnemy<T>(GameObject enemy, float timer) where T : EffectEnemy
    {
        if (enemy.TryGetComponent<T>(out T effect))
        {
            effect.TimerEffect = timer;
            effect.AddEffect();
            return;
        }
        else if (enemy.TryGetComponent<EffectEnemy>(out EffectEnemy effectEnemy))
        {
            effectEnemy.StopEffectEnemy();
        }
        enemy.AddComponent<T>();
    }

    public void StopEffectEnemy()
    {
        Destroy(this);
    }
    protected abstract void AddEffect();
    //protected abstract void OnDestroy();
}
