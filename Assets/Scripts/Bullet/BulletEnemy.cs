﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BulletEnemy : Bullet
{
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.TryGetComponent<TilemapCollider2D>(out TilemapCollider2D tilemapCollider2D))
        {
            HitBullet();
        }
        if (collision.transform.TryGetComponent<IHealthHero>(out IHealthHero hero))
        {
            hero.ToDamageHero(direction * forcePushEnemy, damage);
            HitBullet();
        }
    }
}
