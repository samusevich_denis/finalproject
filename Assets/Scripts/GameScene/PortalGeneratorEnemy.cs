﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PortalGeneratorEnemy : MonoBehaviour
{
    private GameObject prefabPortal = SettingPrefabs.GamePrefabs.Portal;
    private DataWave waveData;
    private Transform[] transformsPortal;

    public void SpawnPortal(DataWave waveData)
    {
        this.waveData = waveData;
        switch (waveData.spawnData.typeSpawn)
        {
            case DataSpawn.TypeSpawning.AllEnemy:
                Spawn((DataSpawnAllEnemy)waveData.spawnData);
                break;
            case DataSpawn.TypeSpawning.Gradually:
                Spawn((DataSpawnGradually)waveData.spawnData);
                break;
            case DataSpawn.TypeSpawning.ByTime:
                Spawn((DataSpawnByTime)waveData.spawnData);
                break;
        }
    }

    private void Spawn(DataSpawnAllEnemy spawnData)
    {
        CreateTransformsPortal(spawnData.quantityPortals);
        var enemies = GetEnemyForPortal(waveData.enemysData);
        for (int i = 0; i < transformsPortal.Length; i++)
        {
            StartCoroutine(InstantiatePortal(transformsPortal[i], enemies[i]));
        }
    }

    private void Spawn(DataSpawnGradually spawnData)
    {
        CreateTransformsPortal(spawnData.quantityPortals);
        var enemies = GetEnemyForPortal(waveData.enemysData);
        StartCoroutine(SpawnGradually(spawnData,enemies));
    }

    private IEnumerator SpawnGradually(DataSpawnGradually spawnData, DataEnemy[][] enemies)
    {
        for (int i = 0; i < transformsPortal.Length; i++)
        {
            int countEnemy = GameController.CountEnemy;
            StartCoroutine(InstantiatePortal(transformsPortal[i], enemies[i]));
            for (int j  = 0; j < enemies[i].Length; j++)
            {
                countEnemy += enemies[i][j].quantityEnemies;
            }
            if (countEnemy > spawnData.maxEnemy)
            {
                while (GameController.CountEnemy > spawnData.minEnemy)
                {
                    yield return new WaitForSeconds(1f);
                }
            }
        }
    }

    private void Spawn(DataSpawnByTime SpawnData)
    {
        CreateTransformsPortal(SpawnData.quantityPortals);
        var enemies = GetEnemyForPortal(waveData.enemysData);
        StartCoroutine(SpawnByTime(SpawnData, enemies));
    }

    private IEnumerator SpawnByTime(DataSpawnByTime spawnData, DataEnemy[][] enemies)
    {
        for (int i = 0; i < transformsPortal.Length; i++)
        {
            int countEnemy = 0;
            StartCoroutine(InstantiatePortal(transformsPortal[i], enemies[i]));
            for (int j = 0; j < enemies[i].Length; j++)
            {
                countEnemy += enemies[i][j].quantityEnemies;
            }
            if (countEnemy > spawnData.quantityEnemySpawnForTime)
            {
                yield return new WaitForSeconds(spawnData.timerSpawn);
            }
        }
    }

    private IEnumerator InstantiatePortal(Transform parent, DataEnemy[] enemysData)
    {
        yield return new WaitForSeconds(Random.Range(1f, 3f));
        var obj = Instantiate(prefabPortal, parent.position, Quaternion.Euler(0f, parent.right.x>0?0f: 180f,0f));
        var portal = obj.GetComponentInChildren<PortalEnemy>();
        while (!portal.IsPortalActive)
        {
            yield return null;
        }
        StartCoroutine(portal.PortalSpawnEnemy(enemysData));
    }

    private void CreateTransformsPortal(int quantity)
    {
        transformsPortal = new Transform[quantity];
        int[] randomNumbers = new int[quantity];
        for (int i = 0; i < randomNumbers.Length; i++)
        {
            bool flagRandom = false;
            do
            {
                flagRandom = false;
                randomNumbers[i] = Random.Range(0, GameController.LevelGenerator.PointsPortal.childCount);
                for (int j = 0; j < i; j++)
                {
                    if (randomNumbers[i] == randomNumbers[j])
                    {
                        flagRandom = true;
                    }
                }
            }
            while (flagRandom);
        }
        for (int i = 0; i < transformsPortal.Length; i++)
        {
            transformsPortal[i] = GameController.LevelGenerator.PointsPortal.GetChild(randomNumbers[i]);
        }
    }

    private DataEnemy[][] GetEnemyForPortal(DataEnemy[] enemysData)
    {
        DataEnemy[][] enemies = new DataEnemy[transformsPortal.Length][];
        for (int i = 0; i < enemies.Length; i++)
        {
            enemies[i] = new DataEnemy[enemysData.Length];
        }
        for (int j = 0; j < enemysData.Length; j++)
        {
            int quantityAllEnemy = enemysData[j].quantityEnemies;
            int quantityForOnePortal = quantityAllEnemy / transformsPortal.Length;
            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i][j] = DataEnemy.CopyDataEnemy(enemysData[j]);
                if (i == enemies.GetLength(0) - 1)
                {
                    enemies[i][j].quantityEnemies = quantityAllEnemy;
                }
                else
                {
                    enemies[i][j].quantityEnemies = quantityForOnePortal;
                    quantityAllEnemy -= quantityForOnePortal;
                }
            }
        }
        return enemies;
    }
}
