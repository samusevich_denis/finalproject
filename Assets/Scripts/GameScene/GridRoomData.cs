﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum VerticalPosition
{
    Up,
    Middle,
    Down,
}

public enum HorizontalPosition
{
    Left,
    Middle,
    Right,
}

public class GridRoomData : MonoBehaviour
{
    private VerticalPosition verticalPosition;
    private HorizontalPosition horizontalPosition;

    public void SetData(VerticalPosition verticalPos, HorizontalPosition horizontalPos)
    {
        verticalPosition = verticalPos;
        horizontalPosition = horizontalPos;
    }

    public void TranslateRoom(VerticalPosition position)
    {
        if (position == VerticalPosition.Down)
        {
            switch (verticalPosition)
            {
                case VerticalPosition.Up:
                    verticalPosition = VerticalPosition.Down;
                    var pos = transform.position;
                    pos.y -= Constants.RoomMedieval.DistanceVertical * 3;
                    transform.position = pos;
                    break;
                case VerticalPosition.Middle:
                    verticalPosition = VerticalPosition.Up;
                    break;
                case VerticalPosition.Down:
                    verticalPosition = VerticalPosition.Middle;
                    break;
            }
        }
        if (position == VerticalPosition.Up)
        {
            switch (verticalPosition)
            {
                case VerticalPosition.Up:
                    verticalPosition = VerticalPosition.Middle;
                    break;
                case VerticalPosition.Middle:
                    verticalPosition = VerticalPosition.Down;
                    break;
                case VerticalPosition.Down:

                    verticalPosition = VerticalPosition.Up;
                    var pos = transform.position;
                    pos.y += Constants.RoomMedieval.DistanceVertical * 3;
                    transform.position = pos;
                    break;
            }
        }
    }

    public bool IsCentralRoom()
    {
        if (verticalPosition == VerticalPosition.Middle&& horizontalPosition == HorizontalPosition.Middle)
        {
            return true;
        }
        return false;
    }

    public void TranslateRoom(HorizontalPosition position)
    {
        if (position == HorizontalPosition.Left)
        {
            switch (horizontalPosition)
            {
                case HorizontalPosition.Right:
                    horizontalPosition = HorizontalPosition.Left;
                    var pos = transform.position;
                    pos.x -= Constants.RoomMedieval.DistanceHorizontal * 3;
                    transform.position = pos;
                    break;
                case HorizontalPosition.Middle:
                    horizontalPosition = HorizontalPosition.Right;
                    break;
                case HorizontalPosition.Left:
                    horizontalPosition = HorizontalPosition.Middle;
                    break;
            }
        }
        if (position == HorizontalPosition.Right)
        {
            switch (horizontalPosition)
            {
                case HorizontalPosition.Right:
                    horizontalPosition = HorizontalPosition.Middle;
                    break;
                case HorizontalPosition.Middle:
                    horizontalPosition = HorizontalPosition.Left;
                    break;
                case HorizontalPosition.Left:
                    horizontalPosition = HorizontalPosition.Right;
                    var pos = transform.position;
                    pos.x += Constants.RoomMedieval.DistanceHorizontal * 3;
                    transform.position = pos;
                    break;
            }
        }
    }
}
