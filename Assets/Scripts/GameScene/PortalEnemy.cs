﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalEnemy : MonoBehaviour
{
    public bool IsPortalActive { get; private set; }
    private Vector3 startScale = Settings.Portal.StartScale;
    private float speedScale = Settings.Portal.SpeedScale;

    void Start()
    {
        transform.localScale = startScale;
        IsPortalActive = false;
        StartCoroutine(PortalActivation());
    }

    private IEnumerator PortalActivation()
    {
        yield return null;
        while (transform.localScale.x<1)
        {
            var scale = transform.localScale;
            scale.x += Time.deltaTime * speedScale;
            transform.localScale = scale;
            yield return null;
        }
        while (transform.localScale.y < 1)
        {
            var scale = transform.localScale;
            scale.y += Time.deltaTime * speedScale;
            transform.localScale = scale;
            yield return null;
        }
        IsPortalActive = true;
    }
    public IEnumerator PortalDeactivation()
    {
        yield return null;
        while (transform.localScale.x < 1)
        {
            var scale = transform.localScale;
            scale.y -= Time.deltaTime * speedScale;
            transform.localScale = scale;
            yield return null;
        }
        while (transform.localScale.y < 1)
        {
            var scale = transform.localScale;
            scale.x -= Time.deltaTime * speedScale;
            transform.localScale = scale;
            yield return null;
        }
        Destroy(transform.parent.gameObject);
    }

    public IEnumerator PortalSpawnEnemy(DataEnemy[] enemyData)
    {
        for (int i = 0; i < enemyData.Length; i++)
        {
            int quantity = enemyData[i].quantityEnemies;
            while (quantity > 0)
            {
                var enemy = Instantiate(enemyData[i].enemyPrefabs, transform.position, Quaternion.identity);
                var ComponentEnemy = enemy.GetComponent<Enemy>();
                float direction = 1;
                if (transform.right.x < 0)
                {
                    direction = -1f;
                }
                ComponentEnemy.SetDataEnemy(enemyData[i], true, direction);
                yield return null;
                yield return new WaitForSeconds(Random.Range(0.2f, 0.8f));
                quantity -= 1;
            }
        }
        StartCoroutine(PortalDeactivation());
    }
}
