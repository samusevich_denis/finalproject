﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InclinedPlatform : MonoBehaviour
{
    [SerializeField] private Vector3 inclined;
    public Vector3 Inclined { get => inclined; private set=> inclined = value; }
    private void Start()
    {
        Inclined = Inclined.normalized;
    }
}
