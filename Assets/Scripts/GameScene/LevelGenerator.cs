﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public Transform PointsPortal { get; private set; }

    private GameObject prefabRoom = SettingPrefabs.GamePrefabs.Room;
    private GameObject prefabPointsPortal = SettingPrefabs.GamePrefabs.PointsPortal;

    private GridRoomData centralRoom;
    private List<GridRoomData> gridRoomsData;
    private Transform heroPlayerTransform;

    void Start()
    {
        heroPlayerTransform = GameController.Hero;
        gridRoomsData = new List<GridRoomData>();
        for (int i = 0; i <3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                gridRoomsData.Add(CreateGridRoomData(prefabRoom, (VerticalPosition)i, (HorizontalPosition)j,Constants.RoomMedieval.StartRoomPosition));
                if (i==1 && j==1)
                {
                    centralRoom = gridRoomsData[gridRoomsData.Count - 1];
                }
            }
        }
        PointsPortal = Instantiate(prefabPointsPortal, centralRoom.transform.position, Quaternion.identity).transform;
    }

    public GridRoomData CreateGridRoomData(GameObject prefabRoom, VerticalPosition verticalPos, HorizontalPosition horizontalPos, Vector3 startPosition)
    {
        var obj = Instantiate(prefabRoom, GetPositionRoom(startPosition, verticalPos, horizontalPos), Quaternion.identity,transform);
        var gridRoomData = obj.GetComponent<GridRoomData>();
        gridRoomData.SetData(verticalPos, horizontalPos);
        return gridRoomData;
    }

    private Vector3 GetPositionRoom(Vector3 startPosition, VerticalPosition verticalPos, HorizontalPosition horizontalPos)
    {
        Vector3 pos = Vector3.zero;
        switch (verticalPos)
        {
            case VerticalPosition.Up:
                pos.y += Constants.RoomMedieval.DistanceVertical;
                break;
            case VerticalPosition.Middle:
                break;
            case VerticalPosition.Down:
                pos.y -= Constants.RoomMedieval.DistanceVertical;
                break;
        }
        switch (horizontalPos)
        {
            case HorizontalPosition.Left:
                pos.x -= Constants.RoomMedieval.DistanceHorizontal;
                break;
            case HorizontalPosition.Middle:
                break;
            case HorizontalPosition.Right:
                pos.x += Constants.RoomMedieval.DistanceHorizontal;
                break;
        }
        pos += startPosition;
        return pos;
    }

    void Update()
    {
        CheckPositionRoom();
    }

    private void CheckPositionRoom()
    {
        var pointHero = centralRoom.transform.InverseTransformPoint(heroPlayerTransform.position);
        if (pointHero.x > Constants.RoomMedieval.DistanceHorizontal * 0.5)
        {
            TranslateRooms(HorizontalPosition.Right);
            return;
        }
        if (pointHero.x < -Constants.RoomMedieval.DistanceHorizontal * 0.5)
        {
            TranslateRooms(HorizontalPosition.Left);
            return;
        }
        if (pointHero.y > Constants.RoomMedieval.DistanceVertical * 0.5)
        {
            TranslateRooms(VerticalPosition.Up);
            return;
        }
        if (pointHero.y < -Constants.RoomMedieval.DistanceVertical * 0.5)
        {
            TranslateRooms(VerticalPosition.Down);
            return;
        }
    }

    private void TranslateRooms(HorizontalPosition position) 
    {
        foreach (var gridRoomData in gridRoomsData)
        {
            gridRoomData.TranslateRoom(position);
            if (gridRoomData.IsCentralRoom())
            {
                centralRoom = gridRoomData;
                PointsPortal.position = centralRoom.transform.position;
            }
        }
    }

    private void TranslateRooms(VerticalPosition position)
    {
        foreach (var gridRoomData in gridRoomsData)
        {
            gridRoomData.TranslateRoom(position);
            if (gridRoomData.IsCentralRoom())
            {
                centralRoom = gridRoomData;
                PointsPortal.position = centralRoom.transform.position;
            }
        }
    }
}
