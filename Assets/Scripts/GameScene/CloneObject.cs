﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CloneObject : MonoBehaviour
{
    private Transform heroPlayerTransform;
    private Transform[] objectsClone;
    private Transform currentTransform;
    private float currentDistance;

    void Start()
    {
        if (GameController.Hero==null)
        {
            Debug.Log("GameController.Hero not found ");
            Destroy(this);
            return;
        }
        heroPlayerTransform = GameController.Hero;
        currentTransform = transform;
        objectsClone = new Transform[8];
        currentDistance = Vector2.Distance(currentTransform.position, heroPlayerTransform.position);
        objectsClone[0] = new GameObject("CloneObjectEnemy").transform; 
        objectsClone[0].position = new Vector3(transform.position.x - Constants.RoomMedieval.DistanceHorizontal, transform.position.y + Constants.RoomMedieval.DistanceVertical); 
        objectsClone[0].parent = transform;

        objectsClone[1] = Instantiate(objectsClone[0].gameObject, new Vector3(transform.position.x, transform.position.y + Constants.RoomMedieval.DistanceVertical), transform.rotation, transform).transform;
        objectsClone[2] = Instantiate(objectsClone[0].gameObject, new Vector3(transform.position.x + Constants.RoomMedieval.DistanceHorizontal, transform.position.y + Constants.RoomMedieval.DistanceVertical), transform.rotation, transform).transform;
        objectsClone[3] = Instantiate(objectsClone[0].gameObject, new Vector3(transform.position.x - Constants.RoomMedieval.DistanceHorizontal, transform.position.y), transform.rotation, transform).transform;
        objectsClone[4] = Instantiate(objectsClone[0].gameObject, new Vector3(transform.position.x + Constants.RoomMedieval.DistanceHorizontal, transform.position.y), transform.rotation, transform).transform;
        objectsClone[5] = Instantiate(objectsClone[0].gameObject, new Vector3(transform.position.x - Constants.RoomMedieval.DistanceHorizontal, transform.position.y - Constants.RoomMedieval.DistanceVertical), transform.rotation, transform).transform;
        objectsClone[6] = Instantiate(objectsClone[0].gameObject, new Vector3(transform.position.x, transform.position.y - Constants.RoomMedieval.DistanceVertical), transform.rotation, transform).transform;
        objectsClone[7] = Instantiate(objectsClone[0].gameObject, new Vector3(transform.position.x + Constants.RoomMedieval.DistanceHorizontal, transform.position.y - Constants.RoomMedieval.DistanceVertical), transform.rotation, transform).transform;
    }

    void Update()
    {
        currentDistance = Vector2.Distance(currentTransform.position, heroPlayerTransform.position);
        for (int i = 0; i < objectsClone.Length; i++)
        {
            float distance = Vector2.Distance(objectsClone[i].position, heroPlayerTransform.position);
            if (distance < currentDistance)
            {
                currentTransform.position = objectsClone[i].position;
                currentDistance = distance;
            }
        }
    }
}
