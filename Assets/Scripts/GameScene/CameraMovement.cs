﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    protected Transform crosshairs;

    void Start()
    {
        crosshairs = GetComponentInChildren<SpriteRenderer>().transform;
    }

    void Update()
    {
        var positionMouseToWorld = InputControllerGame.MousePositionToWorld;
        crosshairs.position = positionMouseToWorld;
        positionMouseToWorld.z = transform.position.z;
        var playerPosition = GameController.Hero.position;
        playerPosition.z = transform.position.z;
        transform.position = Vector3.Lerp(playerPosition, positionMouseToWorld, 0.4f);
    }
}
